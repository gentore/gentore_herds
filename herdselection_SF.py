# -*- coding: utf-8 -*-
"""
Created on Wed May 11 17:40:00 2022

@author: adria036

-------------------------------------------------------------------------------

Script to read data of selected cows, read and select lactation and sensor
feature data for:
    - dataset selected based on herd level: 50-99 % HF

-------------------------------------------------------------------------------

"""

#%% Import packages

import pandas as pd
import os


#%% set  filepaths and constants

# filepath to selected cow data 
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data","herdanalysis/")

# filepath to sensorfeatures
path_sf = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","results/")

# filepath to milk data incl. quantile regression expected
path_my = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data/")

# filenames for purebred and crossbreds and HF selection
fn1 = "dCOWS_50.txt"
fn2 = "dCOWS_99.txt"

# filenames for sf and milkyield data
fnsf = [f for f in os.listdir(path_sf) if os.path.isfile(os.path.join(path_sf,f)) \
             and "D2_CLSFqr" in f ]
fnmy = [f for f in os.listdir(path_my) if os.path.isfile(os.path.join(path_my,f)) \
             and "exp_my" in f ]


#%% read and combine data

# read 50% data
data50 = pd.read_csv(path+fn1, index_col = 0)

# read 99% data
data99 = pd.read_csv(path+fn2, index_col = 0)

# ids of cb and pb to select on
ids_50 = data50["cowid"]
ids_99 = data99["cowid"]


#%% load and select sf data

# loop over my and sf data
for f in fnsf:
    # read data - %timeit result: less than 1s
    temp = pd.read_csv(path_sf+f)
    if "data_sf" not in globals():
        data_sf = temp
    else:
        data_sf = pd.concat([data_sf,temp])
        del temp

# select based on ids by inner merge
sf_50 = pd.merge(data_sf,ids_50, left_on = "CowID", right_on = "cowid")    
sf_50 = sf_50.drop(labels = ["CowID"], axis = 1)  
sf_99 = pd.merge(data_sf,ids_99, left_on = "CowID", right_on = "cowid")    
sf_99 = sf_99.drop(labels = ["CowID"], axis = 1)  
                   

#%% load and select milk yield data

# combine ids to select and optimize memory
ids = pd.concat([ids_50,ids_99])

for f in fnmy:
    #%timeit result: +/- 16s
    temp = pd.read_csv(path_my+f, header = None,
                       names = ["cowid","DIM","MY","expMY"],
                       sep = ' ', na_values="NA")
    temp = pd.merge(temp,ids, left_on = "cowid", right_on = "cowid")    
    if "data_my" not in globals():
        data_my = temp
    else:
        data_my = pd.concat([data_my,temp])
        del temp
        
# select based on ids by inner merge
my_50 = pd.merge(data_my,ids_50, left_on = "cowid", right_on = "cowid")      
my_99 = pd.merge(data_my,ids_99, left_on = "cowid", right_on = "cowid")    

# clear workspace
del fn1, fn2, fnmy, fnsf
del path_my, path_sf

#%% save my and sf data

# purebreds
sf_50.to_csv(path+"dSF_50.txt")
my_50.to_csv(path+"dMY_50.txt")

# crossbreds
sf_99.to_csv(path+"dSF_99.txt")
my_99.to_csv(path+"dMY_99.txt")
