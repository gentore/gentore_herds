# -*- coding: utf-8 -*-
"""
Created on Wed May  4 16:22:05 2022

@author: adria036
------------------------------------------------------------------------------

SELECTION FOR COW/RI/SF ANALYSIS
Given it was not possible to select the herds, the selection is done at cow level
    
    - based on percentage Holstein Friesian blood: <= 50 or >= 90
    - using AFC, meanMY, number of animals in the dataset or in the herd

    - do we need to select cow data as well?
"""

#%% Import packages

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np


#%% Set filepath and read datasets

# filepath to data 
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data/")

# filename
fn = "indicators_9.txt"

# load "cow" data into pandas dataframe
cdata = pd.read_csv(path+fn, header = None, sep = ' ')
# select columns
cdata.drop([2,3,4,9,10,16,17,20,21], axis = 1, inplace = True)
cdata.head(10)

# add colnames
cdata.columns = ["cowid","meanMY","noNA","noNotNA","calfdate","herdid",\
                 "birthdate","breed1","pB1","breed2","pB2","AFC","milksystem"]
   
# drop data of cows with first calving before 2010 & col with AMS info
cdata = cdata.loc[(cdata["calfdate"] > 20100000),:]  
cdata.drop("milksystem",inplace = True, axis = 1)

# drop data of cows not complying to breed criteria
cdata = cdata.loc[((cdata["breed1"].str.match('HF')) & (cdata["pB1"] >= 7)) | \
                   ((cdata["breed2"].str.match('HF')) & (cdata["pB2"] >= 7)) | \
                   ((cdata["breed1"].str.match('HF')) & (cdata["pB1"] == 4)) | \
                   ((cdata["breed2"].str.match('HF')) & (cdata["pB2"] == 4)) | \
                   (((cdata["pB1"] >= 7) | (cdata["pB2"] >= 7)) & \
                   ((~cdata["breed1"].str.match('HF')) & (~cdata["breed2"].str.match('HF')))),:]

# read herd information
fn = "herds_summary.txt"
hdata = pd.read_csv(path+fn, header = None, sep = ' ')
hdata.columns = ["herdid","last_milk","pHF","no1991","no2000","no2001","no2002",
                "no2003","no2004","no2005","no2006","no2007","no2008",
                "no2009","no2010","no2011","no2012","no2013","no2014",
                "no2015","no2016","no2017","no2018","no2019","no2020"]
hdata.head(10)

# assumption: pHF = out of 8/8 so to get true percentage =  divide by 8*100
hdata["pHF"] = hdata["pHF"]/8*100

# sum of all cows over the selected years need to be bigger than 10
hdata["nosum"] = hdata.iloc[:,3:].sum(axis = 1)
hdata["nomax"] = hdata.iloc[:,3:-1].max(axis = 1)
# visualise number of cows per year in the dataset
hdata["nomax"].loc[(hdata["nosum"]>10) & (hdata["nomax"]<300)].hist()

# check how many unique herd ids in the cdata set vs. the herd dataset 
herdids = cdata["herdid"].drop_duplicates().reset_index(drop=True)
fig = plt.figure()
ax = fig.gca()
herds_ids = cdata["herdid"].drop_duplicates()
herds_sel = pd.merge(hdata[["herdid","pHF"]],herds_ids,on = "herdid")
herds_sel["pHF"].hist(ax = ax)
ax.set_title("Herd data herds with cows, percentage Holstein-Friesian")
ax.set_xlabel("percentage HF [%]")
ax.set_ylabel("No. herds")




#%% SELECTION 1 = purebreds/crossbreds
"""
Goal = end up with multiple datasets with cows 
Selection criteria:
    HERD SUMMARIES -- hdata
    - data availablility (and how many not)
    - last_milk after 20110101
    - pHF > 90% (for purely Hostein)
    - no of animals in herd unique per year < 300
    
    COW SUMMARIES -- cdata
    - select cows from animals in HF herd dataset as selected above
    - select cows that comply with the criteria for nonHF breeds + x-breds
    - select herds based on the animals in the latter
    - summarize cow/herd chars
    - select HF farms based on cow/herd chars with KDE function (non-parametric)
"""
#---------------------------SELECT AT HERD LEVEL-------------------------------

# last milking before 20110101 (start = 5799, sel = 5753, 99.2% )
hdata = hdata.loc[hdata["last_milk"] > 20110000,:]

# max no of unique animals => 50 & =< 300 (start = 5753, sel = 5170, 89.9%)
hdata = hdata.loc[((hdata["nomax"] >= 50) & \
                    (hdata["nomax"] <= 300)),:]


#-------------------------SELECT CROSS/PUREBREDS-------------------------------
# select cdata from animals that comply to the "breed" / "herd" criteria
# EITHER purebred animals > 7/8 and NOT HF ==> 3540 animals
purebreds = cdata.loc[((cdata["pB1"] >= 7) & (~cdata["breed1"].str.match('HF'))) |
                      ((cdata["pB2"] >= 7) & (~cdata["breed2"].str.match('HF'))),:]
purebreds = purebreds.reset_index(drop=1)

# OR crossebred animals exactly 4/8 HF and 4/8 OTHER/KNOWN ==> 19660 animals
crossbreds = cdata.loc[((cdata["breed1"].str.match('HF')) & (cdata["pB1"] == 4)) | \
                   ((cdata["breed2"].str.match('HF')) & (cdata["pB2"] == 4)),:]
crossbreds = crossbreds.loc[(~crossbreds["breed1"].str.match('ONB')) & \
                            (~crossbreds["breed2"].str.match('ONB')) & \
                            (crossbreds["pB1"] == 4) & \
                            (crossbreds["pB2"] == 4),:]
    
# both need to comply with herd criteria as well (in terms of number of animals)
purebreds = pd.merge(purebreds,hdata.iloc[:,[0,1,2,-1]],on = "herdid") # 3475  animals = 98.2%
crossbreds = pd.merge(crossbreds,hdata.iloc[:,[0,1,2,-1]],on = "herdid") # 18496 animals = 94.1%

#%% SELECTION 2 = Holstein-Friesian cows

holsteins = cdata.loc[((cdata["breed1"].str.match('HF')) & (cdata["pB1"] >= 7)),:]
holsteins = holsteins.reset_index(drop=1) # 426328 cows
holsteins = pd.merge(holsteins,hdata.iloc[:,[0,1,2,-1]],on = "herdid") # 395654 animals = 92.8%

#---------------------SELECT HOLSTEIN-FRIESIANS--------------------------------
# selection method: sort and uniform sampling
# selection function
def sample_rep(df,nsamples,svars):
    # no of samples selected per iteration
    no_per_iter = round(nsamples/len(svars))
    for i in range(0,len(svars)):
        print("selection = " + svars[i])
        # sort for variable selection
        df = df.sort_values(by = svars[i])
        # determine size of step
        stepsize = int(np.floor(len(df)/no_per_iter))
        # uniform selection of data for this variable
        new = df.iloc[0:len(df):stepsize]
        # concat
        if i == 0:
            sel = new
        else:
            sel = pd.concat([sel,new], axis = 0)
        # delete data that was already selected
        df = df.drop(index = new.index)
    return sel

# number of samples to compare with crossbred animals
nsamples = len(crossbreds)
svars = ["AFC","meanMY","calfdate","nomax"]
cb_holstein = sample_rep(holsteins,nsamples,svars)
cb_holstein = cb_holstein.sort_values(by = "herdid").reset_index(drop=1)

# number of samples to compare with crossbred animals
nsamples = len(purebreds)
svars = ["AFC","meanMY","calfdate","nomax"]
pb_holstein = sample_rep(holsteins,nsamples,svars)
pb_holstein = pb_holstein.sort_values(by = "herdid").reset_index(drop=1)


#%% initial data exploration

print("number of animals in pb dataset = " + str(len(purebreds)))
print("number of unique herds of purebred dataset = " + str(len(purebreds["herdid"].drop_duplicates())) )
print("number of unique herds of counterpart HF purebred dataset = " + str(len(pb_holstein["herdid"].drop_duplicates())) )

print("number of animals in cb dataset = " + str(len(crossbreds)))
print("number of unique herds of crossbred dataset = " + str(len(crossbreds["herdid"].drop_duplicates())) )
print("number of unique herds of counterpart HF purebred dataset = " + str(len(cb_holstein["herdid"].drop_duplicates())) )

# plot MY of PB
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 6))
purebreds["meanMY"].hist(ax=ax,
                     color="teal",
                     alpha = 0.5,
                     lw = 1)  # Age first calving
pb_holstein["meanMY"].hist(ax=ax,
                        color="mediumturquoise",
                        alpha = 0.7,
                        lw = 1)
nocows = len(purebreds)
ax.set_title("purebreds+HF, mean MY, N="+str(nocows), fontsize=14)
ax.set_xlabel("meanMY [kg]", fontsize=12)
ax.set_ylabel("No. cows", fontsize=12)
ax.legend(["non-HF","HF"])

# plot MY of CB
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 6))
crossbreds["meanMY"].hist(ax=ax,
                     color="teal",
                     alpha = 0.5,
                     lw = 1)  # Age first calving
cb_holstein["meanMY"].hist(ax=ax,
                        color="mediumturquoise",
                        alpha = 0.7,
                        lw = 1)
nocows = len(crossbreds)
ax.set_title("crossbreds+HF, mean MY, N="+str(nocows), fontsize=14)
ax.set_xlabel("meanMY [kg]", fontsize=12)
ax.set_ylabel("No. cows", fontsize=12)
ax.legend(["non-HF","HF"])

# plot MY of PB
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 6))
purebreds["AFC"].hist(ax=ax,
                     color="crimson",
                     alpha = 0.5,
                     lw = 1)  # Age first calving
pb_holstein["AFC"].hist(ax=ax,
                        color="purple",
                        alpha = 0.4,
                        lw = 1)
nocows = len(purebreds)
ax.set_title("purebreds+HF, AFC, N="+str(nocows), fontsize=14)
ax.set_xlabel("AFC [months]", fontsize=12)
ax.set_ylabel("No. cows", fontsize=12)
ax.legend(["non-HF","HF"])

# plot MY of CB
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6, 6))
crossbreds["AFC"].hist(ax=ax,
                     color="crimson",
                     alpha = 0.5,
                     lw = 1)  # Age first calving
cb_holstein["AFC"].hist(ax=ax,
                        color="purple",
                        alpha = 0.4,
                        lw = 1)
nocows = len(crossbreds)
ax.set_title("crossbreds+HF, AFC, N="+str(nocows), fontsize=14)
ax.set_xlabel("AFC [months]", fontsize=12)
ax.set_ylabel("No. cows", fontsize=12)
ax.legend(["non-HF","HF"])


#%% Output the datasets for GB
"""
Output .txt files for:
    - purebreds + pb_holstein
    - crossbreds + cb_holstein

"""


#%% Export datasets

# filepath to data 
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data/")

# filename
fn1 = "dCOWS_PB.txt"
fn2 = "dCOWS_PB_HF.txt"  # selection of HF for PB counterpart dataset
fn3 = "dCOWS_CB.txt"
fn4 = "dCOWS_CB_HF.txt"  # selection of HF for CB counterpart dataset

# save data:
"""
cowid, meanMY, noNA, noNOTNA, calfdate, herdid, birthdate, breed1, pB1, 
breed2, pB2, AFC, last_milk*, pHF*, nomax*
    * these  are herd variables
"""
purebreds.to_csv(path+fn1)
pb_holstein.to_csv(path+fn2)
crossbreds.to_csv(path+fn3)
cb_holstein.to_csv(path+fn4)


