# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 09:03:40 2022

@author: adria036
-------------------------------------------------------------------------------

Script for analysis of herd data for GenTORE project

Research question: do herds with very high (>99%) and very low (<50%) HF 
                   differ from one another in terms of production characterist-
                   tics, sensor features?
                   
-------------------------------------------------------------------------------

Analysis will (1) visualise and (2) statistically compare:
    - 


--> this script does the statistiscal comparisons


"""
import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\gentore\scripts\gentore_herds") 

#%% load packages

import pandas as pd
import os
import matplotlib.pyplot as plt
from openpyxl import load_workbook
from scipy import stats
#from statsmodels.stats.anova import AnovaRM
import numpy as np
import seaborn as sns


#%% set constants, filepaths and read data

# set path
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data","herdanalysis")

path_res = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","results","herdanalysis")


for f in os.listdir(path):
    print(f)
    
# load data cows
cowid50 = pd.read_csv(path+"\dCOWS_50.txt",
                    index_col = 0)
cowid50["herdpart"] = 50
cowid99 = pd.read_csv(path+"\dCOWS_99.txt",
                    index_col = 0)
cowid99["herdpart"] = 99
cowid = pd.concat([cowid50,cowid99], axis = 0) 
del cowid50, cowid99

# load data herds
herds50 = pd.read_csv(path+"\dHERDS_50.txt",
                    index_col = 0)
herds50["herdpart"] = 50
herds99 = pd.read_csv(path+"\dHERDS_99.txt",
                    index_col = 0)
herds99["herdpart"] = 99
herds = pd.concat([herds50,herds99], axis = 0) 
del herds50, herds99

# load data MY
milk50 = pd.read_csv(path+"\dMY_50.txt",
                    index_col = 0)
milk50["herdpart"] = 50
milk99 = pd.read_csv(path+"\dMY_99.txt",
                    index_col = 0)
milk99["herdpart"] = 99
milk = pd.concat([milk50,milk99], axis = 0) 
del milk50, milk99

# load data SF
sf50 = pd.read_csv(path+"\dSF_50.txt",
                    index_col = 0)
sf50["herdpart"] = 50
sf99 = pd.read_csv(path+"\dSF_99.txt",
                    index_col = 0)
sf99["herdpart"] = 99
sf = pd.concat([sf50,sf99], axis = 0) 
del sf50, sf99
del path, f

# add % HF per cow to dataframe
cowid["pHF"] = 0
cowid.loc[cowid["breed1"] == "HF","pHF"] = cowid.loc[cowid["breed1"] == "HF","pB1"]
cowid.loc[cowid["breed2"] == "HF","pHF"] = cowid.loc[cowid["breed2"] == "HF","pB2"]
cowid["pHF"] = cowid["pHF"]/8*100


#%% summary of the data in a table for presentations or publications

# summary stats based on herd
temp_avg = herds[["pHF","nomax","herdpart"]].groupby(by = "herdpart").mean().transpose()
temp_std = herds[["pHF","nomax","herdpart"]].groupby(by = "herdpart").std().transpose()
temp_min = herds[["pHF","nomax","herdpart"]].groupby(by = "herdpart").min().transpose()
temp_max = herds[["pHF","nomax","herdpart"]].groupby(by = "herdpart").max().transpose()
# combine and reorder
tempHERD = pd.concat([temp_avg[50],temp_std[50],temp_min[50],temp_max[50], \
                  temp_avg[99],temp_std[99],temp_min[99],temp_max[99]], axis = 1)

# Display number of herds
print("total number of herds  = " + str(len(herds)))
print("total number of herds in SEL50 = " + str(len(herds.loc[herds["herdpart"] == 50,:])))
print("total number of herds in SEL99 = " + str(len(herds.loc[herds["herdpart"] == 99,:])))
print("total number of cows  = " + str(len(cowid)))
print("total number of cows in SEL50 = " + str(len(cowid.loc[cowid["herdpart"] == 50,:])))
print("total number of herds in SEL99 = " + str(len(cowid.loc[cowid["herdpart"] == 99,:])))

# summary stats based on cowid (per herd)
anicounts = cowid[["pHF","herdid"]].groupby("herdid").count()
anicounts["anicounts"] = anicounts["pHF"]
anicounts = anicounts.drop("pHF", axis = 1)
# mean average herd
temp1 = cowid[["AFC","meanMY","noNotNA","pHF","herdpart","herdid"]].groupby("herdid").mean()
temp1["herdpart"] = temp1["herdpart"].astype("int")
temp1 = pd.concat([temp1,anicounts],axis = 1)
temp_avg = temp1.groupby(by = "herdpart").mean().transpose()
# std average herd
temp1 = cowid[["AFC","meanMY","noNotNA","pHF","herdpart","herdid"]].groupby("herdid").mean()
temp1["herdpart"] = temp1["herdpart"].astype("int")
temp1 = pd.concat([temp1,anicounts],axis = 1)
temp_std = temp1.groupby(by = "herdpart").std().transpose()
# min average herd
temp1 = cowid[["AFC","meanMY","noNotNA","pHF","herdpart","herdid"]].groupby("herdid").mean()
temp1["herdpart"] = temp1["herdpart"].astype("int")
temp1 = pd.concat([temp1,anicounts],axis = 1)
temp_min = temp1.groupby(by = "herdpart").min().transpose()
# max average herd
temp1 = cowid[["AFC","meanMY","noNotNA","pHF","herdpart","herdid"]].groupby("herdid").mean()
temp1["herdpart"] = temp1["herdpart"].astype("int")
temp1 = pd.concat([temp1,anicounts],axis = 1)
temp_max = temp1.groupby(by = "herdpart").max().transpose()

# combine and reorder
tempCOW = pd.concat([temp_avg[50],temp_std[50],temp_min[50],temp_max[50], \
                     temp_avg[99],temp_std[99],temp_min[99],temp_max[99]], axis = 1)

# combine tempCOW and tempHERD
summary_stats = pd.concat([tempHERD,tempCOW],axis = 0)
summary_stats.columns = ['avg_50', 'std_50', 'min_50', 'max_50', \
                         'avg_99', 'std_99', 'min_99', 'max_99']

del temp1,temp_std, temp_avg, temp_min, temp_max, tempCOW, tempHERD

# filename
fn = "\Ex_herdanalysis.xlsx"
wb = "Herd_overview"

# create attendance list from existing file
excel_book = load_workbook(path_res+fn)
with pd.ExcelWriter(path_res+fn, engine='openpyxl') as writer:
    writer.book = excel_book
    writer.sheets = {
        worksheet.title: worksheet
        for worksheet in excel_book.worksheets
        }
    summary_stats.to_excel(writer, 
                  sheet_name = wb, 
                  index = True)
    writer.save()

del writer, wb, fn, excel_book, anicounts


#%% Statistical analysis I 

#----------- milk production ttest ---------

# milk yield
my_compare = []
for dim in range(11,341):
    a = milk.loc[(milk["DIM"] == dim) & \
                 (milk["herdpart"] == 50),"MY"]
    b = milk.loc[(milk["DIM"] == dim) & \
                 (milk["herdpart"] == 99),"MY"]
    t,p = stats.ttest_ind(a,b,nan_policy ='omit')
    my_compare.append([t,p])
# RESULT : all significant with a p-value below 0.001
    
# milk yield
emy_compare = []
for dim in range(11,341):
    a = milk.loc[(milk["DIM"] == dim) & \
                 (milk["herdpart"] == 50),"expMY"]
    b = milk.loc[(milk["DIM"] == dim) & \
                 (milk["herdpart"] == 99),"expMY"]
    t,p = stats.ttest_ind(a,b,nan_policy ='omit')
    emy_compare.append([t,p])
# RESULT: all significant with a p-value below 0.001   
    
del t,p, dim, a, b

#------------------ sensor features - excluded-------------------
# number of lactations for which no SF can be calculated
print("Number excluded in SEL50 = " + str(len(sf.loc[(sf["Include"]==0) & \
                                                     (sf["herdpart"]==50),:])))
print("Percentage excluded in SEL50 = " + str(100*len(sf.loc[(sf["Include"]==0) & \
                                                     (sf["herdpart"]==50),:]) 
                                              / len(sf.loc[sf["herdpart"]==50,:])))
    
print("Number excluded in SEL99 = " + str(len(sf.loc[(sf["Include"]==0) & \
                                                     (sf["herdpart"]==99),:])))
print("Percentage excluded in SEL99 = " + str(100*len(sf.loc[(sf["Include"]==0) & \
                                                     (sf["herdpart"]==99),:]) 
                                              / len(sf.loc[sf["herdpart"]==99,:])))
no_exclude = sf.loc[sf["Include"] == 0,:]  # 4068
gaps_excl = no_exclude.loc[no_exclude["LargestGap"] > 5,:] # 2250
minday_excl = no_exclude.loc[no_exclude["DIMfirst"] > 5,:]  # 1070
maxday_excl = no_exclude.loc[no_exclude["DIMlast"] < 150,:] # 945

# clean up workspace
del no_exclude, gaps_excl,minday_excl,maxday_excl


#%% add LnVAR
# add lnVAR
milk["RES"] = milk["MY"] - milk["expMY"]
test = milk[["RES","cowid"]].groupby(by="cowid").var()
test["LnVAR"] = np.log(test["RES"])
test = test.drop("RES",axis = 1)
sf = sf.join(test, on = "cowid")

# select based on "include" variable
sf = sf.loc[sf["Include"]==1,:]  # 41052 -> 36974
del test

#%% visualise main breed

SEL50 = cowid.loc[cowid["herdpart"] == 50,:]
test = SEL50[["breed1","cowid"]].groupby(by = "breed1").count()
test2 = SEL50[["breed1","meanMY"]].groupby(by = "breed1").mean()
test3 = SEL50[["breed1","meanMY"]].groupby(by = "breed1").std()
test2["std"] = test3["meanMY"]

# filename
fn = "\Ex_herdanalysis.xlsx"
wb = "Breed_overview_SEL50"

# create attendance list from existing file
excel_book = load_workbook(path_res+fn)
with pd.ExcelWriter(path_res+fn, engine='openpyxl') as writer:
    writer.book = excel_book
    writer.sheets = {
        worksheet.title: worksheet
        for worksheet in excel_book.worksheets
        }
    # test.to_excel(writer, 
    #               sheet_name = wb, 
    #               index = True)
    test2.to_excel(writer,
                   sheet_name = "amy",
                   index = True)
    writer.save()



del writer, wb, fn, excel_book, test

#%% delete outliers per variable (not per cow)
for variable in sf.columns:
    if variable not in ["herdpart","DIMfirst","DIMlast","NoGaps"]:
        print(variable)
        # for SEL99
        a = sf.loc[(sf["herdpart"]==99),variable].mean() + 6*sf.loc[(sf["herdpart"]==99),variable].std()
        b = sf.loc[(sf["herdpart"]==99),variable].mean() - 6*sf.loc[(sf["herdpart"]==99),variable].std()
    
        sf.loc[((sf[variable] > a) | \
                     (sf[variable] < b)) & \
                     (sf["herdpart"] == 99), variable] = np.nan
        
        # for SEL50
        a = sf.loc[(sf["herdpart"]==50),variable].mean() + 6*sf.loc[(sf["herdpart"]==50),variable].std()
        b = sf.loc[(sf["herdpart"]==50),variable].mean() - 6*sf.loc[(sf["herdpart"]==50),variable].std()
    
        sf.loc[((sf[variable] > a) | \
                     (sf[variable] < b)) & \
                     (sf["herdpart"] == 50), variable] = np.nan


#%% dataset preparation for statistical modelling

# 1) summarize breeds + count breeds
breeds = cowid[["breed1","cowid"]].groupby(by="breed1").count()
breeds = breeds.loc[breeds.cowid > 50]
breeds = breeds.reset_index()
breeds = breeds.drop("cowid",axis = 1)
breeds = pd.concat([breeds,pd.DataFrame({"breed1" : ["CROSS"]})], ignore_index = True)

# add to sensor features the breedcategory
cowid["breed"] = "ONB"

for i in range(0,len(breeds)-1):
    print(breeds["breed1"][i])
    cowid.loc[((cowid["pB1"] >= 5) & \
               (cowid["breed1"] == breeds["breed1"][i])),"breed"] = breeds["breed1"][i]

cowid.loc[(((cowid["pB1"] == 4) & \
            (cowid["breed1"] == "HF")) | \
           ((cowid["pB2"] == 4) & \
            (cowid["breed2"] == "HF"))),"breed"] = "CROSS"

newframe = cowid[["cowid","breed"]].copy()
sf = sf.merge(newframe,how="inner",on="cowid",right_index=False)
del newframe

# add milk production per breed category
mybreed = cowid[["breed","meanMY"]].groupby(by="breed").mean()
mybreed = mybreed.reset_index()
mybreed.rename(columns = {"meanMY" : "breedMY"},inplace=True)

sf = sf.merge(mybreed, how = "outer", on = "breed", right_index = False)


# 2) summarize farms and drop sf of farms with less than 5 animals (needed to calculate random effects)
herdids = cowid[["herdid","cowid"]].groupby(by = "herdid").count()
herdids = herdids.sort_values(by = "cowid")
herdids = herdids.loc[herdids["cowid"]>=5,:].reset_index()
herdids = herdids.drop(columns= ["cowid"])
cowids2 = cowid.merge(herdids,how="inner",right_index = False, on = "herdid")
newframe = cowid[["cowid","herdid"]].copy()
sf = sf.merge(newframe,how="inner",right_index = False, on = "cowid")
del newframe, cowids2
sf = sf.sort_values(by=["cowid","herdid"])



#%% statistical differences across SF, not taking herd effect nor scaling into account

# visualise distributions of sensor features / resilience indicators
#    to this end, we make different categories, and select the most interesting 
#    features.
# parameters of the quantile regression models
set1 = ["parsQR0","parsQR1","parsQR2","parsQR3","parsQR4","parsQR5"] 
# resilience indicators
set2 = ["ExpAClag1","LnVAR"]
# lactation curve characteristics of WOOD (fit, not expected)
set3 = ["Wood305","Wood50","WoodPeakDIM","WoodPeak","WoodSlopeToPeak", "WoodPersistency"]
# characteristics of the residuals of WOOD (fit, not expected)
set4 = ["WoodAutoCorr","WoodVariance","WoodMaxResid","WoodMeanResid","WoodSkewResid","WoodMeanAbsResid"] 
# characterstics of expected lactation curve
set5 = ["ExpPeakDIM","ExpPeak","ExpSlopeToPeak","ExpPersistency"]
# residual characteristics of expected lactation curve (QR)
set6 = ["ExpRMSE","ExpPercNegative","ExpPercLower85","ExpNeg_5d","ExpNeg_10d","ExpMaxResid", \
        "ExpAutoCorr","ExpVariance","ExpMeanResid","ExpSkewResid","ExpMeanAbsResid"]
# pertubation characteristics
set7 = ["PertNoMajor","PertNoMinor","PertNoTotal","PertMajDaysRec","PertMajDaysDev",\
        "PertMinDaysRec","PertMinDaysDev","PertMajMilkLoss","PertMinMilkLoss",\
        "PertTotalLoss","PertDeepest"]
  
    
    

#%% test: stats model with herd correction and my correction + outlier detection with cooks
#------------------------------------------------------------------------------
# STEP1: set up model without standardisation
# STEP2: calculate Cooks' distances
# STEP3: remove outliers based on Cook's distance criteria
# STEP4: standardise data, also breedMY with min-max standardisation
# STEP5: set up model and analyse
#------------------------------------------------------------------------------


import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels import robust

#STEP0: example = with "ExpRMSE" - select variables and group in "data"
data = sf[["ExpRMSE","herdpart","breedMY","herdid"]].copy()
data = data.dropna()   # crucial
data = data.reset_index(drop=1)

# define which cols are categorical
data["herdpart"] = data["herdpart"].astype("category")
data["herdid"] = data["herdid"].astype("category")

# add intercept
data["intercept"] = 1


#STEP1: linear model, not taking random effects into account for Cook's distance

# fit model
lm = sm.OLS(data["ExpRMSE"],data[["intercept","herdpart","breedMY"]]).fit()
print(lm.summary())

# create instance of influence   -- # np.set_printoptions(suppress=True)
influence = lm.get_influence()

# obtain Cook's distance for each observation
cooks = influence.cooks_distance

# select outliers based on Cook's distance larger than the threshold of q=99.5%
q = np.quantile(cooks[0],q=0.995)
outl = cooks[0][cooks[0] > q]
print("number of outliers = " + str(len(outl)))

# plot outlying observations based on threshold q
plt.rc('font', size=15)
fig,ax = plt.subplots(nrows = 2, ncols = 1, figsize = (10,15),sharex = False)
plotx = np.arange(1,len(cooks[0])+1,1)
ax[0].scatter(plotx,cooks[0],color = 'b',s = 3)
ax[0].scatter(plotx[cooks[0]>q],outl,s=12,linewidth = 1,marker = "x",color = 'r')
ax[0].set_xlabel('datapoint')
ax[0].set_ylabel('Cook''s distance')
ax[0].set_title("Cook's distances, q = " + str(round(q,4)))


# remove outliers with cook's distance larger than the quantile q = 0.995 
data2 = data.iloc[np.where(cooks[0]<q)].copy()
# min-max standardisation sf and milk yield averages per breed
data2["ExpRMSE"] = (data2["ExpRMSE"] - data2["ExpRMSE"].min()) / \
                     (data2["ExpRMSE"].max() - data2["ExpRMSE"].min())  # standardise
data2["breedMY"] = (data2["breedMY"] - data2["breedMY"].min()) / \
                      (data2["breedMY"].max() - data2["breedMY"].min())  # standardise
                      
# run LMER
md = smf.mixedlm("ExpRMSE ~ herdpart + breedMY", data2, groups=data2["herdid"])
mdf = md.fit(method=["lbfgs"])
mdf.cov_params()
print(mdf.summary())
# data2[]
# get predicted
predict = mdf.predict()


# plot fitted vs observed
import matplotlib.colors as colors
import matplotlib.cm as cmx
uniq = list(set(data2['breedMY']))
z = range(1,len(uniq))
hsv = plt.get_cmap('hsv')
cNorm  = colors.Normalize(vmin=0, vmax=len(uniq))
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=hsv)

fig,ax = plt.subplots(nrows = 2, ncols = 1, figsize = (10,15),sharex = False)
for i in range(len(uniq)):
    indx = data2['breedMY'] == uniq[i]
    ax[1].scatter(data2["ExpRMSE"][indx],predict[indx],color = scalarMap.to_rgba(i),s = 3)
ax[1].legend(uniq)
ax[1].set_xlabel('datapoint')
ax[1].set_ylabel('Cook''s distance')
ax[1].set_title("Cook's distances, q = " + str(round(q,4)))




data2["ExpRMSE"].loc[data2["herdpart"]==50].hist(alpha = 0.5,density=True)
data2["ExpRMSE"].loc[data2["herdpart"]==99].hist(alpha = 0.5,density=True)

data2["ExpRMSE_s"].loc[data2["herdpart"]==50].boxplot()
data2["ExpRMSE_s"].loc[data2["herdpart"]==99].boxplot()

fig,ax = plt.subplots(nrows = 1, ncols = 1, figsize = (60,30),sharex = True)
sns.boxplot(x = "herdpart", 
                 y = "ExpRMSE",
                 hue = "herdpart", 
                 data = data2, 
                 #color = ["#56B4E9","#0072B2"],
                 palette = "colorblind",
                 showfliers = True,
                 ax = ax)



"""
# second method
endog = data2["ExpRMSE_s"]
data2["intercept"] = 1
exog = data2[["intercept","herdpart","breedMY"]]
md = sm.MixedLM(endog,exog, groups=data2["herdid"],exog_re=exog["intercept"])
mdf = md.fit(method=["lbfgs"])
print(mdf.summary())

# other options for cooks distance interpretation (remove more data):
    # mad = robust.mad(cooks[0], c=1)
    # outl = cooks[0][cooks[0] > threshold*mad]
    # outl = cooks[0][cooks[0] > 3*np.mean(cooks[0])]
    # outl = cooks[0][cooks[0] > (4/len(cooks[0]))]
# plots
    # plt.plot(plotx,q*np.ones(len(plotx)),linewidth = 0.5,color = 'r')
    # plt.scatter(plotx[cookdist>3*np.mean(cookdist)],outl,s=4,marker = "x",color = 'm')
# add ancova with pingouin lib
import pingouin
anc = pingouin.ancova(data=data2, 
                dv = "PertDeepest",
                between = "herdpart",
                covar = "MeanMY")
# gives similar results as the regression analysis
"""
    

#%% implement stats analysis for all sensor features
import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels.formula.api import ols
# from statsmodels import robust




# sf list
sfset = ["ExpAClag1","LnVAR","Wood305","Wood50","WoodPeakDIM","WoodPeak",\
              "WoodSlopeToPeak", "WoodPersistency","WoodAutoCorr","WoodVariance",\
              "WoodMaxResid","WoodMeanResid","WoodSkewResid","WoodMeanAbsResid",\
              "ExpPeakDIM","ExpPeak","ExpSlopeToPeak","ExpPersistency",\
              "ExpRMSE","ExpPercNegative","ExpPercLower85","ExpNeg_5d","ExpNeg_10d",\
              "ExpMaxResid", "ExpAutoCorr","ExpVariance","ExpMeanResid",\
              "ExpSkewResid","ExpMeanAbsResid","PertNoMajor","PertNoMinor",\
              "PertNoTotal","PertMajDaysRec","PertMajDaysDev",\
              "PertMinDaysRec","PertMinDaysDev","PertMajMilkLoss","PertMinMilkLoss",\
              "PertTotalLoss","PertDeepest"]

statsall = pd.DataFrame([],columns = sfset,index = ["IC","herdpart","MY","p_IC","p_herdpart","p_MY","R2"])

for feat in sfset:
    
    #STEP0: select variables and group in "data"
    data = sf[[feat,"herdpart","MeanMY","breedMY","herdid"]].copy()
    data = data.dropna()   # crucial
    data = data.reset_index(drop=1)
    
    # define which cols are categorical
    data["herdpart"] = data["herdpart"].astype("category")
    data["herdid"] = data["herdid"].astype("category")
    
    # add intercept
    data["intercept"] = 1


    #STEP1: linear model, not taking random effects into account for Cook's distance

    # fit model
    lm = sm.OLS(data[feat],data[["intercept","herdpart","MeanMY"]]).fit()
    print(lm.summary())
    # create instance of influence   -- # np.set_printoptions(suppress=True)
    influence = lm.get_influence()
    # obtain Cook's distance for each observation
    cooks = influence.cooks_distance

    # select outliers based on Cook's distance larger than the threshold of q=99.5%
    q = np.quantile(cooks[0],q=0.995)
    outl = cooks[0][cooks[0] > q]
    print("number of outliers = " + str(len(outl)))

    # plot outlying observations based on threshold q
    plt.rc('font', size=20)
    fig,ax = plt.subplots(nrows = 2, ncols = 2, figsize = (30,25),sharex = False)
    plotx = np.arange(1,len(cooks[0])+1,1)
    ax[0][0].scatter(plotx,cooks[0],color = 'darkslategrey',s = 3)
    ax[0][0].scatter(plotx[cooks[0]>q],outl,s=12,linewidth = 1,marker = "x",color = 'r')
    ax[0][0].set_xlabel('datapoint')
    ax[0][0].set_ylabel('Cook''s distance')
    ax[0][0].set_title("Cook's distances, q = " + str(round(q,4)) + ", no. obs = " + str(len(outl)))

    # remove outliers with cook's distance larger than the quantile q = 0.995 
    data2 = data.iloc[np.where(cooks[0]<q)].copy()
    # min-max standardisation sf and milk yield averages per breed
    data2[feat] = (data2[feat] - data2[feat].min()) / \
                         (data2[feat].max() - data2[feat].min())  # standardise
    data2["MeanMY"] = (data2["MeanMY"] - data2["MeanMY"].min()) / \
                          (data2["MeanMY"].max() - data2["MeanMY"].min())  # standardise
                      
    # # run LM
    # mdl = sm.OLS(data2[feat],data2[["intercept","herdpart","MeanMY"]]).fit()
    # print(mdl.summary())
    # fitted = mdl.fittedvalues

    # run LM with categorical defined
    eq = feat + ' ~ C(herdpart, Sum) + MeanMY'
    cat_lm = ols(feat + ' ~ C(herdpart, Sum) + MeanMY',
                 data=data2).fit()
    fitted = cat_lm.fittedvalues
    table = sm.stats.anova_lm(cat_lm, typ=2) # Type 2 ANOVA DataFrame
    print(table)
    print(cat_lm.summary())
    
    # plot
    ax[1][1].scatter(data2.loc[data2["herdpart"]==99,feat],fitted.loc[data2["herdpart"]==99],s = 3, color = 'orange')
    ax[1][1].scatter(data2.loc[data2["herdpart"]==50,feat],fitted.loc[data2["herdpart"]==50],s = 3, color = 'blue')
    ax[1][1].legend(["SEL99","SEL50"])
    ax[1][1].plot([0, 1], [0 ,1],linewidth = 1, color = 'r')
    ax[1][1].set_xlabel('observed')
    ax[1][1].set_ylabel('fitted')
    ax[1][1].set_title(feat + ", fitted vs observed, p{SEL} = " + str(round(cat_lm.pvalues[1],4)))
    
    
    sns.boxplot(x = "herdpart", 
                     y = feat,
                     hue = "herdpart", 
                     data = data2, 
                     #color = ["#FF8000","#0000FF"],
                     palette = "colorblind",
                     showfliers = True,
                     ax = ax[0][1])
    
    data2[feat].loc[data2["herdpart"]==50].hist(density=True, color = 'blue', ax = ax[1][0])
    data2[feat].loc[data2["herdpart"]==99].hist(alpha = 0.8,density=True, color = 'orange', ax = ax[1][0])
    ax[1][0].legend(["SEL50","SEL99"]) 
    ax[1][0].set_xlabel("standardised " + feat)
    ax[1][0].set_ylabel("density")
    
    # plt.savefig(path_res+"mod_" +feat+".jpg")

    
    # write to tables
    statsall[feat]["p_IC"] = round(cat_lm.pvalues[0],4)
    statsall[feat]["p_herdpart"] = round(cat_lm.pvalues[1],4)
    statsall[feat]["p_MY"] = round(cat_lm.pvalues[2],4)
    statsall[feat]["R2"] = round(cat_lm.rsquared,4)
    statsall[feat]["IC"] = round(cat_lm.params[0],4)
    statsall[feat]["herdpart"] = round(cat_lm.params[1],4)
    statsall[feat]["MY"] = round(cat_lm.params[2],4)

# filename
fn = "\Ex_herdanalysis.xlsx"
wb = "lm_stats"

# create attendance list from existing file
excel_book = load_workbook(path_res+fn)
with pd.ExcelWriter(path_res+fn, engine='openpyxl') as writer:
    writer.book = excel_book
    writer.sheets = {
        worksheet.title: worksheet
        for worksheet in excel_book.worksheets
        }
    statsall.to_excel(writer, 
                  sheet_name = wb, 
                  index = True)
    writer.save()

del writer, wb, fn, excel_book
del data2,ax,breeds,cat_lm, cooks, outl, fitted, table, eq, feat, fig, herdids
del herds, i, influence, lm, mybreed, plotx, q
del sfset



#%% HF analsysis
import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels.formula.api import ols

cowid["isHF"] = 0
cowid.loc[((cowid["breed1"].str.match('HF')) & (cowid["pB1"] >= 7)) | \
          ((cowid["breed2"].str.match('HF')) & (cowid["pB2"] >= 7)), "isHF"] = 1
    
# select HF cows
HFcow = cowid.loc[cowid["isHF"] == 1,:]
HFsf = pd.merge(sf,HFcow["cowid"], how = "inner", on = "cowid" )

# lactation curve of HF cows
sfset = ["Wood305","Wood50","WoodPeakDIM","WoodPeak",\
         "WoodSlopeToPeak", "WoodPersistency","WoodAutoCorr",
         "ExpPercNegative","ExpPercLower85","ExpNeg_5d","ExpNeg_10d",\
         "PertNoMajor","PertNoMinor","PertNoTotal","PertMajDaysRec","PertMajDaysDev",\
         "PertMinDaysRec","PertMinDaysDev"]

HFstats = pd.DataFrame([],index = sfset,columns = ["feat","medSEL50","medSEL99"])

for feat in sfset:
    print(feat)
    med1 = round(HFsf.loc[HFsf["herdpart"] == 50,feat].median(),3)
    med2 = round(HFsf.loc[HFsf["herdpart"] == 99,feat].median(),3)
    
    HFstats["medSEL50"][feat] = med1
    HFstats["medSEL99"][feat] = med2
    
    plt.rc('font', size=14)
    fig, ax = plt.subplots(ncols=2,nrows=1,figsize = (14,7))
    sns.boxplot(x = "herdpart",
                y = feat, 
                hue = "herdpart", 
                data = HFsf, 
                palette = "colorblind",
                showfliers = True,
                ax = ax[0])
    sns.violinplot(x = "herdpart",
                y = feat, 
                hue = "herdpart", 
                data = HFsf, 
                palette = "colorblind",
                ax = ax[1])
    ax[0].set_xticks([], [])
    ax[1].set_xticks([], [])
    ax[0].set_title(feat + ", SEL50 = " + str(med1) + ", SEL99 = " + str(med2))
    ax[1].set_title(feat + ", SEL50 = " + str(med1) + ", SEL99 = " + str(med2))
    ax[0].grid()
    ax[1].grid()
    # save results
    plt.savefig(path_res+"\\"+"sf_onlyHF_"+variable+".png")


# overview table of data in HF dataset
HFcow[["herdpart","meanMY"]].groupby(by="herdpart").count()
HFcow[["herdpart","meanMY","AFC"]].groupby(by="herdpart").mean()
HFcow[["herdpart","meanMY","AFC"]].groupby(by="herdpart").std()
HFsf[["herdpart","NoDays"]].groupby(by="herdpart").mean()
HFsf[["herdpart","NoDays"]].groupby(by="herdpart").std()

# statistical differences

sfset = ["ExpAClag1","LnVAR","Wood305","Wood50","WoodPeakDIM","WoodPeak",\
              "WoodSlopeToPeak", "WoodPersistency","WoodAutoCorr","WoodVariance",\
              "WoodMaxResid","WoodMeanResid","WoodSkewResid","WoodMeanAbsResid",\
              "ExpPeakDIM","ExpPeak","ExpSlopeToPeak","ExpPersistency",\
              "ExpRMSE","ExpPercNegative","ExpPercLower85","ExpNeg_5d","ExpNeg_10d",\
              "ExpMaxResid", "ExpAutoCorr","ExpVariance","ExpMeanResid",\
              "ExpSkewResid","ExpMeanAbsResid","PertNoMajor","PertNoMinor",\
              "PertNoTotal","PertMajDaysRec","PertMajDaysDev",\
              "PertMinDaysRec","PertMinDaysDev","PertMajMilkLoss","PertMinMilkLoss",\
              "PertTotalLoss","PertDeepest"]
statsHFall = pd.DataFrame([],columns = sfset,index = ["IC","herdpart","MY","p_IC","p_herdpart","p_MY","R2"])
for feat in sfset:
    
    #STEP0: select variables and group in "data"
    data = HFsf[[feat,"herdpart","MeanMY","breedMY","herdid"]].copy()
    data = data.dropna()   # crucial
    data = data.reset_index(drop=1)
    
    # define which cols are categorical
    data["herdpart"] = data["herdpart"].astype("category")
    data["herdid"] = data["herdid"].astype("category")
    
    # add intercept
    data["intercept"] = 1


    #STEP1: linear model, not taking random effects into account for Cook's distance

    # fit model
    lm = sm.OLS(data[feat],data[["intercept","herdpart","MeanMY"]]).fit()
    print(lm.summary())
    # create instance of influence   -- # np.set_printoptions(suppress=True)
    influence = lm.get_influence()
    # obtain Cook's distance for each observation
    cooks = influence.cooks_distance

    # select outliers based on Cook's distance larger than the threshold of q=99.5%
    q = np.quantile(cooks[0],q=0.995)
    outl = cooks[0][cooks[0] > q]
    print("number of outliers = " + str(len(outl)))

    # plot outlying observations based on threshold q
    plt.rc('font', size=20)
    fig,ax = plt.subplots(nrows = 2, ncols = 2, figsize = (30,25),sharex = False)
    plotx = np.arange(1,len(cooks[0])+1,1)
    ax[0][0].scatter(plotx,cooks[0],color = 'darkslategrey',s = 3)
    ax[0][0].scatter(plotx[cooks[0]>q],outl,s=12,linewidth = 1,marker = "x",color = 'r')
    ax[0][0].set_xlabel('datapoint')
    ax[0][0].set_ylabel('Cook''s distance')
    ax[0][0].set_title("Cook's distances, q = " + str(round(q,4)) + ", no. obs = " + str(len(outl)))

    # remove outliers with cook's distance larger than the quantile q = 0.995 
    data2 = data.iloc[np.where(cooks[0]<q)].copy()
    # min-max standardisation sf and milk yield averages per breed
    data2[feat] = (data2[feat] - data2[feat].min()) / \
                         (data2[feat].max() - data2[feat].min())  # standardise
    data2["MeanMY"] = (data2["MeanMY"] - data2["MeanMY"].min()) / \
                          (data2["MeanMY"].max() - data2["MeanMY"].min())  # standardise

    # run LM with categorical defined
    eq = feat + ' ~ C(herdpart, Sum) + MeanMY'
    cat_lm = ols(feat + ' ~ C(herdpart, Sum) + MeanMY',
                 data=data2).fit()
    fitted = cat_lm.fittedvalues
    table = sm.stats.anova_lm(cat_lm, typ=2) # Type 2 ANOVA DataFrame
    print(table)
    print(cat_lm.summary())
    
    # plot
    ax[1][1].scatter(data2.loc[data2["herdpart"]==99,feat],fitted.loc[data2["herdpart"]==99],s = 3, color = 'orange')
    ax[1][1].scatter(data2.loc[data2["herdpart"]==50,feat],fitted.loc[data2["herdpart"]==50],s = 3, color = 'blue')
    ax[1][1].legend(["SEL99","SEL50"])
    ax[1][1].plot([0, 1], [0 ,1],linewidth = 1, color = 'r')
    ax[1][1].set_xlabel('observed')
    ax[1][1].set_ylabel('fitted')
    ax[1][1].set_title(feat + ", fitted vs observed, p{SEL} = " + str(round(cat_lm.pvalues[1],4)))
    
    
    sns.boxplot(x = "herdpart", 
                     y = feat,
                     hue = "herdpart", 
                     data = data2, 
                     #color = ["#FF8000","#0000FF"],
                     palette = "colorblind",
                     showfliers = True,
                     ax = ax[0][1]
                     )
    ax[0][1].set_title = "HF cows only"
    
    data2[feat].loc[data2["herdpart"]==50].hist(density=True, color = 'blue', ax = ax[1][0])
    data2[feat].loc[data2["herdpart"]==99].hist(alpha = 0.8,density=True, color = 'orange', ax = ax[1][0])
    ax[1][0].legend(["SEL50","SEL99"]) 
    ax[1][0].set_xlabel("standardised " + feat)
    ax[1][0].set_ylabel("density")
    
    # plt.savefig(path_res+"mod_" +feat+".jpg")

    
    # write to tables
    statsHFall[feat]["p_IC"] = round(cat_lm.pvalues[0],4)
    statsHFall[feat]["p_herdpart"] = round(cat_lm.pvalues[1],4)
    statsHFall[feat]["p_MY"] = round(cat_lm.pvalues[2],4)
    statsHFall[feat]["R2"] = round(cat_lm.rsquared,4)
    statsHFall[feat]["IC"] = round(cat_lm.params[0],4)
    statsHFall[feat]["herdpart"] = round(cat_lm.params[1],4)
    statsHFall[feat]["MY"] = round(cat_lm.params[2],4)

# filename
fn = "\Ex_herdanalysis.xlsx"
wb = "HF_lm_stats"

# create attendance list from existing file
excel_book = load_workbook(path_res+fn)
with pd.ExcelWriter(path_res+fn, engine='openpyxl') as writer:
    writer.book = excel_book
    writer.sheets = {
        worksheet.title: worksheet
        for worksheet in excel_book.worksheets
        }
    statsHFall.to_excel(writer, 
                  sheet_name = wb, 
                  index = True)
    writer.save()

del writer, wb, fn, excel_book
del data2,ax,breeds,cat_lm, cooks, outl, fitted, table, eq, feat, fig, herdids
del herds, i, influence, lm, mybreed, plotx, q
del sfset































#%% create table with results of stats
"""
# 1) test normality
# 2) test equal variances
# 3) test Welch's differences
"""
def f_test(group1, group2):
    f = np.var(group1, ddof=1)/np.var(group2, ddof=1)
    nun = group1.size-1
    dun = group2.size-1
    p_value = 1-stats.f.cdf(f, nun, dun)
    return f, p_value
  
# prepare outlier deletion
sf = sf.loc[sf["Include"]==1,:]  # 41052 -> 36974
for variable in sf.columns:
    if variable not in ["herdpart","DIMfirst","DIMlast","NoGaps"]:
        print(variable)
        # for SEL99
        a = sf.loc[(sf["herdpart"]==99),variable].mean() + 6*sf.loc[(sf["herdpart"]==99),variable].std()
        b = sf.loc[(sf["herdpart"]==99),variable].mean() - 6*sf.loc[(sf["herdpart"]==99),variable].std()
    
        sf.loc[((sf[variable] > a) | \
                     (sf[variable] < b)) & \
                     (sf["herdpart"] == 99), variable] = np.nan
        
        # for SEL50
        a = sf.loc[(sf["herdpart"]==50),variable].mean() + 6*sf.loc[(sf["herdpart"]==50),variable].std()
        b = sf.loc[(sf["herdpart"]==50),variable].mean() - 6*sf.loc[(sf["herdpart"]==50),variable].std()
    
        sf.loc[((sf[variable] > a) | \
                     (sf[variable] < b)) & \
                     (sf["herdpart"] == 50), variable] = np.nan

# z-scoresper sensor feature!
zsf = sf.copy()
for variable in sf.columns:
    if variable not in ["herdpart","DIMfirst","DIMlast","NoGaps","cowid"]:
        print(variable)
        m = sf[variable].quantile(q=0.1)  # calculate 10th quantile
        M = sf[variable].quantile(q=0.9)  # calculate 90th quantile
        
        # calculate z-score
        zsf[variable] = (sf[variable]-m)/(M-m)
        

del a,b, m, M









#%%
statsr  = pd.DataFrame([])
sfset = [set1, set2, set3, set4, set5, set5, set6, set7]
for sfn in sfset:
    for variable in sfn:
       
        # select the data & drop nan
        group1 = sf.loc[sf["herdpart"]==50,variable]
        group2 = sf.loc[sf["herdpart"]==99,variable]
        group1 = group1.dropna()
        group2 = group2.dropna()        
        
        #-------------- no scale corrected, nor herd corrected-----------------
        # perform the F test (alternative: var group 1 4x different var group2)
        F, pval1 = f_test(group1, group2)
        pval1 = round(pval1,4)
        
        # perform the Welch test if unequal variances
        if pval1 < 0.05:
            # welch
            tt,pval2 = stats.ttest_ind(group1, group2, equal_var = False)
        else:
            # ttest
            tt, pval2 = stats.ttest_ind(group1, group2, equal_var = True)
        pval2 = round(pval2,4)
                
        #--------------- scale corrected, not herd corrected-------------------
        my1 = sf.loc[sf["herdpart"] == 50, 'MeanMY']
        my1 = my1.mean()
        my2 = sf.loc[sf["herdpart"] == 99, 'MeanMY']
        my2 = my2.mean()
        
        # F test remains the same
        
        # perform the Welch test if unequal variances
        if pval1 < 0.05:
            # welch
            tt,pval3 = stats.ttest_ind(group1/my1, group2/my2, equal_var = False)
        else:
            # ttest
            tt, pval3 = stats.ttest_ind(group1/my1, group2/my2, equal_var = True)
        pval3 = round(pval3,4)
        
        # production individual scaling corrected by mean daily milk yield
        # select the data & drop nan
        group3 = sf.loc[sf["herdpart"]==50,variable]/sf.loc[sf["herdpart"]==50,"MeanMY"]
        group4 = sf.loc[sf["herdpart"]==99,variable]/sf.loc[sf["herdpart"]==99,"MeanMY"]
        group3 = group3.dropna()
        group4 = group4.dropna()       
        tt,pval4 = stats.ttest_ind(group3, group4, equal_var = False)
        
        # add to table
        statsr = pd.concat([statsr,pd.DataFrame({'sf' : variable, 
                                     'no50' : [len(group1)],
                                     'no99' : [len(group2)],
                                     'mean50':[],
                                     'se50' : [],
                                     'mean99': [],
                                     'se99' : [],
                                     'F' : [pval1],
                                     'Wtt' : [pval2],
                                     'Wttc' : [pval3],
                                     'Wttc_ind' : [pval4]
                                     })])
        
        print(variable,pval1, pval2, pval3)
        
#%% correction factors

# herd milk production
herdmilk = cowid[["herdid","meanMY","herdpart"]].groupby(by = "herdid").mean()

# select HF cows from dataset
cowid["isHF"] = 0
cowid.loc[((cowid["breed1"].str.match('HF')) & (cowid["pB1"] >= 7)) | \
          ((cowid["breed2"].str.match('HF')) & (cowid["pB2"] >= 7)), "isHF"] = 1


    
#%% per herd-breed group
# 0- determine to which breed a cow belongs
#       5/8 or more: breedgroup
#       if 4/8-4/8 HF: separate group of "CB"
# 1- summarize per breed per herd - number of cows in each group
# 2- select if lower than 5 animals
# 3- model with breed/herd random effect

breedset = cowid["breed1"].drop_duplicates().reset_index(drop=1)
cowid["breedpercentage"] = cowid["pB1"]/( cowid["pB1"]+cowid["pB2"])*100
for breed in breedset:
    cowid.loc[(cowid["breed1"] == breed) & \
              (cowid["breedpercentage"] > 50),"breedcat"] = breed
cowid.loc[((cowid["breed1"] == 'HF') | \
           (cowid["breed2"] == 'HF')) & \
           (cowid["breedpercentage"] == 50),"breedcat"] = "cross"
        
herdcat = cowid[["herdid","breedcat","cowid"]].groupby(by = ["herdid","breedcat"]).count()
print("the number of herdcat with less than 5 animals = " +str(sum(herdcat["cowid"] < 5)))
print("this is " + str(round(sum(herdcat["cowid"] < 5) / len(herdcat) *100,1)) + "% of the herd breed categories")
print("the total number of animals deleted = " +str(sum(herdcat.loc[herdcat["cowid"] < 5,"cowid"])))
print("this is " + str(round(sum(herdcat.loc[herdcat["cowid"] < 5,"cowid"]) / sum(herdcat["cowid"]) *100,1)) + "% of the animals")

# remove herdcats with less than 5
herdcat = herdcat.loc[herdcat["cowid"] > 5,:]
herdcat["randcat"] = np.linspace(1,len(herdcat),len(herdcat))
herdcat = herdcat.drop("cowid", axis = 1)
herdcat.reset_index(inplace=True)

test = cowid.merge(herdcat,on = ["herdid","breedcat"], how = "inner")

# modellingvia statsmodels

"""
import researchpy as rp
import statsmodels.api as sm
import scipy.stats as stats
import statsmodels.formula.api as smf
md = smf.mixedlm("alcdep ~ Gambling",groups="Gambling",data = df).fit()

md.summary()
"""