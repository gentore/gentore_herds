# -*- coding: utf-8 -*-
"""
Created on Wed May  4 11:53:52 2022

@author: adria036
------------------------------------------------------------------------------

SELECTION FOR HERD ANALYSIS
    - based on percentage Holstein Friesian blood: <= 50 or >= 90
    - using AFC, meanMY, number of animals in the dataset or in the herd

    - do we need to select cow data as well?

"""

#%% Import packages

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
import copy


#%% Set filepath and read datasets

# filepath to data 
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data/")

# filename
fn = "indicators_9.txt"

# load "cow" data into pandas dataframe
cdata = pd.read_csv(path+fn, header = None, sep = ' ')
# select columns
cdata.drop([2,3,4,9,10,16,17,20,21], axis = 1, inplace = True)
cdata.head(10)

# add colnames
cdata.columns = ["cowid","meanMY","noNA","noNotNA","calfdate","herdid",\
                 "birthdate","breed1","pB1","breed2","pB2","AFC","milksystem"]
   
# drop data of cows with first calving before 2010 & col with AMS info
cdata = cdata.loc[(cdata["calfdate"] > 20100000),:]  
cdata.drop("milksystem",inplace = True, axis = 1)

# read herd information
fn = "herds_summary.txt"
hdata = pd.read_csv(path+fn, header = None, sep = ' ')
hdata.columns = ["herdid","last_milk","pHF","no1991","no2000","no2001","no2002",
                "no2003","no2004","no2005","no2006","no2007","no2008",
                "no2009","no2010","no2011","no2012","no2013","no2014",
                "no2015","no2016","no2017","no2018","no2019","no2020"]
hdata.head(10)

# assumption: pHF = out of 8/8 so to get true percentage =  divide by 8*100
hdata["pHF"] = hdata["pHF"]/8*100

# check how many unique herd ids in the cdata set vs. the herd dataset 
herdids = cdata["herdid"].drop_duplicates().reset_index(drop=True)   # 4053
fig = plt.figure()
ax = fig.gca()
herds_ids = cdata["herdid"].drop_duplicates()
herds_sel = pd.merge(hdata[["herdid","pHF"]],herds_ids,on = "herdid")
herds_sel["pHF"].hist(ax = ax)
ax.set_title("Herd data herds with cows, percentage Holstein-Friesian")
ax.set_xlabel("percentage HF [%]")
ax.set_ylabel("No. herds")

# number of herds with pHF <= 50 in datasets hdata and herds_sel
no_pHF50 = hdata.loc[(hdata["pHF"]<=50),:] # 315
no_pHF50f = herds_sel.loc[(herds_sel["pHF"]<=50),:]  # 209
fig = plt.figure()
ax = fig.gca()
no_pHF50f["pHF"].hist(ax = ax)
noherds = len(no_pHF50f)
ax.set_title("Herd data herds pHF <= 50, percentage Holstein-Friesian, N="+str(noherds))
ax.set_xlabel("percentage HF [%]")
ax.set_ylabel("No. herds")

del ax, fig, fn, path

        
#%% SELECTION 1: HERDS with %HF<50%
# selection all herds and cows from herds with pHF <= 50%
dHERD_50 = pd.merge(hdata,herds_ids,on = "herdid") # all with cowdata -> 4053
dHERD_50 = dHERD_50.loc[(dHERD_50["pHF"]<=50),:]  # with pHF <= 50 -> 209
dHERD_50 = dHERD_50.iloc[:,[0,1,2,15,16,17,18,19,20,21,22,23,24]] # all data
dHERD_50 = dHERD_50.reset_index(drop=1)  # reset index
dHERD_50["nomax"] = dHERD_50.iloc[:,3:-1].max(axis = 1) # add max no of cows
dHERD_50 = dHERD_50.loc[(dHERD_50["nomax"]>=50) & \
                        (dHERD_50["nomax"]<=300),:]  # no cow >= 50 -> 180 FINAL

# plot pHF and no cows
fig, ax = plt.subplots(nrows = 1, ncols = 2, figsize = (15,8))
noherds = len(dHERD_50)
dHERD_50["pHF"].hist(ax = ax[0], color = "mediumblue")
ax[0].set_title("Herds pHF <= 50, %HF, N="+str(noherds),fontsize = 12)
ax[0].set_xlabel("pHF [%]")
ax[0].set_ylabel("No. herds")
dHERD_50["nomax"].hist(ax = ax[1], color = "darkblue")
ax[1].set_title("Herds pHF <= 50, no. cows/y, N="+str(noherds),
                fontsize = 12)
ax[1].set_xlabel("No. of animals")
ax[1].set_ylabel("No. herds")

# select cow dataset
dCOW_50 = pd.merge(cdata,dHERD_50["herdid"]) # 15387 animals
dCOW_50 = dCOW_50.sort_values(by = ["herdid","cowid"])

# visualise breeds
fig, ax = plt.subplots(nrows = 1, ncols = 3, figsize = (28,8))
dCOW_50["breed1"].loc[dCOW_50["pB1"] >= 7].hist(ax = ax[0],
                                                color = "firebrick") # purebred animals
dCOW_50["breed1"].loc[dCOW_50["pB1"] < 7].hist(ax = ax[1],
                                                color = "tomato")
dCOW_50["breed2"].loc[dCOW_50["pB1"] < 7].hist(ax = ax[2],
                                                color = "tomato")
no_cows = len(dCOW_50["breed1"].loc[dCOW_50["pB1"] >= 7])
ax[0].set_title("PB cows [pB>=7], breed, N="+str(no_cows),fontsize = 12)
ax[0].set_xlabel("Breed")
ax[0].set_ylabel("No. cows")
no_cows = len(dCOW_50["breed1"].loc[dCOW_50["pB1"] < 7])
ax[1].set_title("non-PB cows [pB<7], breed1, N="+str(no_cows),
                fontsize = 14)
ax[2].set_title("non-PB cows [pB<7], breed2, N="+str(no_cows),
                fontsize = 14)
ax[1].set_xlabel("Breed", fontsize = 12)
ax[2].set_xlabel("Breed", fontsize = 12)
ax[1].set_ylabel("No. cows", fontsize = 12)
ax[2].set_ylabel("No. cows", fontsize = 12)

# summarize cow dataset by herd
hcounts = dCOW_50.groupby(by = "herdid", axis = 0).count()
hsums = dCOW_50.groupby(by = "herdid", axis = 0).sum()
hmeans = dCOW_50.groupby(by = "herdid", axis = 0).mean()

sHERD_50 = copy.deepcopy(hmeans[["AFC","meanMY","pB1","pB2"]])
sHERD_50["no_animals"] = hcounts["cowid"]
sHERD_50 = sHERD_50.reset_index(drop=1)
sHERD_50["herdid"] = hmeans.index
sHERD_50 = pd.merge(sHERD_50,dHERD_50[["herdid","nomax","last_milk","pHF"]],on = "herdid")

# visualise herd characteristics
fig, ax = plt.subplots(nrows = 1, ncols = 3, figsize = (28,8))
sHERD_50["AFC"].hist(ax = ax[0],
                     color = "limegreen") # Age first calving
sHERD_50["meanMY"].hist(ax = ax[1],
                        color = "darkmagenta")
sHERD_50["no_animals"].hist(ax = ax[2],
                            color = "darkorange")
noherds = len(sHERD_50)
ax[0].set_title("Herds pHF<50, mean AFC, N="+str(no_cows),fontsize = 14)
ax[0].set_xlabel("AFC [months]", fontsize = 12)
ax[0].set_ylabel("No. herds", fontsize = 12)
ax[1].set_title("Herds pHF<50, mean MY, N="+str(no_cows),
                fontsize = 14)
ax[1].set_xlabel("Mean MY [kg]", fontsize = 12)
ax[1].set_ylabel("No. herds", fontsize = 12)
ax[2].set_title("Herds pHF<50, no. animals, N="+str(no_cows),
                fontsize = 14)
ax[2].set_xlabel("Number of animals", fontsize = 12)
ax[2].set_ylabel("No. herds", fontsize = 12)


#%% SELECTION 2: herds with pHF > 99%

# number of herds with pHF >= 99 in datasets hdata and herds_sel
no_pHF99 = hdata.loc[(hdata["pHF"]>=99),:] # 594
no_pHF99f = herds_sel.loc[(herds_sel["pHF"]>=99),:]  # 424
        # from these 424, we want to select 200 herds based on no of cows available
        # and on average milk production (representative sample)
        # and on age at first calving

dHERD_99 = pd.merge(hdata,herds_ids,on = "herdid") # all with cowdata -> 4053
dHERD_99 = dHERD_99.loc[(dHERD_99["pHF"]>=99),:]  # with pHF >= 99 -> 424
dHERD_99 = dHERD_99.iloc[:,[0,1,2,15,16,17,18,19,20,21,22,23,24]] # all data
dHERD_99 = dHERD_99.reset_index(drop=1)  # reset index
dHERD_99["nomax"] = dHERD_99.iloc[:,3:-1].max(axis = 1) # add max no of cows
dHERD_99 = dHERD_99.loc[(dHERD_99["nomax"]>=50) & \
                        (dHERD_99["nomax"]<=300),:]  # no cow >= 50 -> 406 FINAL

# plot pHF and no cows
fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(15, 8))
noherds = len(dHERD_99)
dHERD_99["pHF"].hist(ax=ax[0], color="mediumblue")
ax[0].set_title("Herds pHF >= 99, %HF, N="+str(noherds), fontsize=12)
ax[0].set_xlabel("pHF [%]")
ax[0].set_ylabel("No. herds")
dHERD_99["nomax"].hist(ax=ax[1], color="darkblue")
ax[1].set_title("Herds pHF >= 99, no. cows/y, N="+str(noherds),
                fontsize=12)
ax[1].set_xlabel("No. of animals")
ax[1].set_ylabel("No. herds")

# select cow dataset
dCOW_99 = pd.merge(cdata, dHERD_99["herdid"])  # 57861 animals
dCOW_99 = dCOW_99.sort_values(by=["herdid", "cowid"])

# visualise breeds
fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(28, 8))
dCOW_99["breed1"].loc[dCOW_99["pB1"] >= 7].hist(ax=ax[0],
                                                color="firebrick")  # purebred animals
dCOW_99["breed1"].loc[dCOW_99["pB1"] < 7].hist(ax=ax[1],
                                               color="tomato")
dCOW_99["breed2"].loc[dCOW_99["pB1"] < 7].hist(ax=ax[2],
                                               color="tomato")
no_cows = len(dCOW_99["breed1"].loc[dCOW_99["pB1"] >= 7])
ax[0].set_title("PB cows [pB>=7], breed, N="+str(no_cows), fontsize=12)
ax[0].set_xlabel("Breed")
ax[0].set_ylabel("No. cows")
no_cows = len(dCOW_99["breed1"].loc[dCOW_99["pB1"] < 7])
ax[1].set_title("non-PB cows [pB<7], breed1, N="+str(no_cows),
                fontsize=14)
ax[2].set_title("non-PB cows [pB<7], breed2, N="+str(no_cows),
                fontsize=14)
ax[1].set_xlabel("Breed", fontsize=12)
ax[2].set_xlabel("Breed", fontsize=12)
ax[1].set_ylabel("No. cows", fontsize=12)
ax[2].set_ylabel("No. cows", fontsize=12)

# summarize cow dataset by herd
hcounts = dCOW_99.groupby(by="herdid", axis=0).count()
hsums = dCOW_99.groupby(by="herdid", axis=0).sum()
hmeans = dCOW_99.groupby(by="herdid", axis=0).mean()

sHERD_99 = copy.deepcopy(hmeans[["AFC", "meanMY", "pB1", "pB2"]])
sHERD_99["no_animals"] = hcounts["cowid"]
sHERD_99 = sHERD_99.reset_index(drop=1)
sHERD_99["herdid"] = hmeans.index
sHERD_99 = pd.merge(sHERD_99,dHERD_99[["herdid","nomax","last_milk","pHF"]],on = "herdid")

# visualise herd characteristics
fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(28, 8))
sHERD_99["AFC"].hist(ax=ax[0],
                     color="limegreen")  # Age first calving
sHERD_99["meanMY"].hist(ax=ax[1],
                        color="darkmagenta")
sHERD_99["no_animals"].hist(ax=ax[2],
                            color="darkorange")
noherds = len(sHERD_99)
ax[0].set_title("Herds pHF>=99, mean AFC, N="+str(no_cows), fontsize=14)
ax[0].set_xlabel("AFC [months]", fontsize=12)
ax[0].set_ylabel("No. herds", fontsize=12)
ax[1].set_title("Herds pHF>=99, mean MY, N="+str(no_cows),
                fontsize=14)
ax[1].set_xlabel("Mean MY [kg]", fontsize=12)
ax[1].set_ylabel("No. herds", fontsize=12)
ax[2].set_title("Herds pHF>=99, no. animals, N="+str(no_cows),
                fontsize=14)
ax[2].set_xlabel("Number of animals", fontsize=12)
ax[2].set_ylabel("No. herds", fontsize=12)

#TODO: select herds with 99% hF based on KDE 
# representative sample for milk production, number of animals, herdsize and AFC

# selection method: sort and uniform sampling
nsamples = 180
svars = ["AFC","meanMY","no_animals","nomax"]
def sample_rep(df,nsamples,svars):
    # no of samples selected per iteration
    no_per_iter = round(nsamples/len(svars))
    for i in range(0,len(svars)):
        print("selection = " + svars[i])
        # sort for variable selection
        df = df.sort_values(by = svars[i])
        # determine size of step
        stepsize = int(np.floor(len(df)/no_per_iter))
        # uniform selection of data for this variable
        new = df.iloc[0:len(df):stepsize]
        # concat
        if i == 0:
            sel = new
        else:
            sel = pd.concat([sel,new], axis = 0)
        # delete data that was already selected
        df = df.drop(index = new.index)
    return sel

# select data
dHERD_99_sel = sample_rep(sHERD_99,nsamples,svars) # 181 herds
dCOW_99_sel = pd.merge(dCOW_99,dHERD_99_sel["herdid"]) # 25655 cows
dHERD_99_selHerd = pd.merge(dHERD_99,dHERD_99_sel["herdid"],
                            how = "inner")

#TODO: compare distributions
dHERD_99_sel["AFC"].hist()

#TODO compare scatterplots

#%% Export datasets

# filepath to data 
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data","herdanalysis/")

# filename
fn1 = "dHERDS_50.txt"
fn2 = "dHERDS_99.txt"
fn3 = "dCOWS_50.txt"
fn4 = "dCOWS_99.txt"

# save
dHERD_50.to_csv(path+fn1)
dHERD_99_selHerd.to_csv(path+fn2)
dCOW_50.to_csv(path+fn3)
dCOW_99_sel.to_csv(path+fn4)
