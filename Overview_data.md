## Dataset description GenTORE

Explanation and variables from the selection of GenTORE data (analysis April-July 2022)

### Datasets

**1. herdselection**

- dHERDS_50.txt
- dHERDS_99.txt

_Variables_
    herdid
	last_milk
	pHF
	no2011
	no2012
	no2013
	no2014
	no2015
	no2016
	no2017
	no2018
	no2019
	no2020
	nomax

- dCOWS_50.txt
- dCOWS_99.txt
	
_Variables_
	cowid
	meanMY
	noNA
	noNotNA
	calfdate
	herdid
	birthdate
	breed1
	pB1
	breed2
	pB2
	AFC
	last_milk
	pHF
	nomax

- dMY_50.txt
- dMY_99.txt

_Variables_
	cowid
	DIM
	MY
	expMY

- dSF_50.txt
- dSF_99.txt
    
_Variables_
    all sensor features

**2. breedselection**

- dCOWS_PB.txt
- dCOWS_PB_HF.txt
- dCOWS_CB.txt
- dCOWS_CB_HF.txt
	
_Variables_
	cowid
	meanMY
	noNA
	noNotNA
	calfdate
	herdid
	birthdate
	breed1
	pB1
	breed2
	pB2
	AFC
	last_milk
	pHF
	nomax

- dMY_CB.txt
- dMY_PB.txt

_Variables_
	cowid
	DIM
	MY
	expMY

- dSF_CB.txt
- dSF_PB.txt
    
_Variables_
    all sensor features