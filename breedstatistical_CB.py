# -*- coding: utf-8 -*-
"""
Created on Mon Nov  7 21:57:47 2022

@author: adria036
"""
import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\gentore\scripts\gentore_herds") 

#%% load packages

import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from openpyxl import load_workbook
from scipy import stats
#from statsmodels.stats.anova import AnovaRM
import numpy as np
import seaborn as sns


#%% set constants, filepaths and read data

# set path
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data","breedanalysis")

path_res = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","results","breedanalysis")

# load data selection  for crossbred comparison
cowidCB = pd.read_csv(path+"\dCOWS_CB.txt",
                    index_col = 0)
cowidCB["breed"] = "CB"     # cross breds
cowidCBHF = pd.read_csv(path+"\dCOWS_CB_HF.txt",
                    index_col = 0)
cowidCBHF["breed"] = "HF"    #HF pure breds
cowid = pd.concat([cowidCB,cowidCBHF], axis = 0) 
del cowidCB, cowidCBHF

# load data SF
sf = pd.read_csv(path+"\dSF_CB.txt",
                    index_col = 0)

sf = sf.merge(cowid[["cowid","breed","herdid"]])

# load milk yield data
milk = pd.read_csv(path+"\dMY_CB.txt",
                    index_col = 0)
milk = milk.merge(cowid[["cowid","breed"]])

# add LnVAR & autocorr
milk["RES"] = milk["MY"] - milk["expMY"]
test = milk[["RES","cowid"]].groupby(by="cowid").var()
test["LnVAR"] = np.log(test["RES"])
test = test.drop("RES",axis = 1)
sf = sf.join(test, on = "cowid")
del test

# exclude if Include = 0
sf = sf.loc[sf["Include"] == 1,:]   #♣ deletes 3545


#%% milk production

# milk yield
my_compare = []
for dim in range(11,341):
    a = milk.loc[(milk["DIM"] == dim) & \
                 (milk["breed"] == "CB"),"MY"]
    b = milk.loc[(milk["DIM"] == dim) & \
                 (milk["breed"] == "HF"),"MY"]
    t,p = stats.ttest_ind(a,b,nan_policy ='omit')
    my_compare.append([t,p])
# RESULT : all significant with a p-value below 0.001
    
# milk yield
emy_compare = []
for dim in range(11,341):
    a = milk.loc[(milk["DIM"] == dim) & \
                 (milk["breed"] == "CB"),"expMY"]
    b = milk.loc[(milk["DIM"] == dim) & \
                 (milk["breed"] == "PB"),"expMY"]
    t,p = stats.ttest_ind(a,b,nan_policy ='omit')
    emy_compare.append([t,p])
# RESULT: all significant with a p-value below 0.001   
    
del t,p, dim, a, b



#%% statistical analysis

import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels.formula.api import ols

cmap = cm.get_cmap('Spectral')
# sf list
sfset = ["ExpAClag1","LnVAR","Wood305","Wood50","WoodPeakDIM","WoodPeak",\
              "WoodSlopeToPeak", "WoodPersistency","WoodAutoCorr","WoodVariance",\
              "WoodMaxResid","WoodMeanResid","WoodSkewResid","WoodMeanAbsResid",\
              "ExpPeakDIM","ExpPeak","ExpSlopeToPeak","ExpPersistency",\
              "ExpRMSE","ExpPercNegative","ExpPercLower85","ExpNeg_5d","ExpNeg_10d",\
              "ExpMaxResid", "ExpAutoCorr","ExpVariance","ExpMeanResid",\
              "ExpSkewResid","ExpMeanAbsResid","PertNoMajor","PertNoMinor",\
              "PertNoTotal","PertMajDaysRec","PertMajDaysDev",\
              "PertMinDaysRec","PertMinDaysDev","PertMajMilkLoss","PertMinMilkLoss",\
              "PertTotalLoss","PertDeepest"]
rasset = sf["breed"].drop_duplicates().to_list()
    

statsall = pd.DataFrame([],columns = sfset,index = ["IC","breed","MY","p_IC","p_breed","p_MY","R2"])


for feat in sfset:
    
    #STEP0: select variables and group in "data"
    data = sf[[feat,"breed","MeanMY","herdid"]].copy()
    data = data.dropna()   # crucial
    data = data.reset_index(drop=1)

    # define which cols are categorical
    data["breed"] = data["breed"].astype("category")
    
    # add intercept
    data["intercept"] = 1

    #STEP1: linear model, not taking random effects into account for Cook's distance
    # run LM with categorical defined
    eq = feat + ' ~ C(breed, Sum) + MeanMY'
    lm = ols(feat + ' ~ C(breed, Sum) + MeanMY',
             data=data).fit()
    # fit model
    #lm = sm.OLS(data[feat],data[["intercept","breed","MeanMY"]]).fit()
    print(lm.summary())
    # create instance of influence   -- # np.set_printoptions(suppress=True)
    influence = lm.get_influence()
    # obtain Cook's distance for each observation
    cooks = influence.cooks_distance

    # select outliers based on Cook's distance larger than the threshold of q=99.5%
    q = np.quantile(cooks[0],q=0.995)
    outl = cooks[0][cooks[0] > q]
    print("number of outliers = " + str(len(outl)))

    # plot outlying observations based on threshold q
    plt.rc('font', size=20)
    fig,ax = plt.subplots(nrows = 2, ncols = 2, figsize = (30,25),sharex = False)
    plotx = np.arange(1,len(cooks[0])+1,1)
    ax[0][0].scatter(plotx,cooks[0],color = 'darkslategrey',s = 3)
    ax[0][0].scatter(plotx[cooks[0]>q],outl,s=12,linewidth = 1,marker = "x",color = 'r')
    ax[0][0].set_xlabel('datapoint')
    ax[0][0].set_ylabel('Cook''s distance')
    ax[0][0].set_title("Cook's distances, q = " + str(round(q,4)) + ", no. obs = " + str(len(outl)))

    # remove outliers with cook's distance larger than the quantile q = 0.995 
    data3 = data.iloc[np.where(cooks[0]>q)].copy()
    data2 = data.iloc[np.where(cooks[0]<q)].copy()
    # min-max standardisation sf and milk yield averages
    data2[feat] = (data2[feat] - data2[feat].min()) / \
                         (data2[feat].max() - data2[feat].min())  # standardise
    data2["MeanMY"] = (data2["MeanMY"] - data2["MeanMY"].min()) / \
                          (data2["MeanMY"].max() - data2["MeanMY"].min())  # standardise
         
    # run LM with categorical defined
    #eq = feat + ' ~ C(breed, Sum) + MeanMY'
    cat_lm = ols(feat + ' ~ C(breed, Sum) + MeanMY',
                 data=data2).fit()
    fitted = cat_lm.fittedvalues
    table = sm.stats.anova_lm(cat_lm, typ=2) # Type 2 ANOVA DataFrame
    print(table)
    print(cat_lm.summary())
    
    md = smf.mixedlm(feat + " ~ C(breed,Sum) + C(herdid,Sum) + MeanMY", data2, groups=data2["herdid"])
    mdf = md.fit(method=["lbfgs"])
    mdf.cov_params()            
    print(mdf.summary())
    predict = mdf.predict()
    
    # plot
    ax[1][1].scatter(data2.loc[data2["breed"]=="HF",feat],fitted.loc[data2["breed"]=="HF"],s = 3, color = 'orange')
    ax[1][1].scatter(data2.loc[data2["breed"]=="CB",feat],fitted.loc[data2["breed"]=="CB"],s = 3, color = 'blue')
    ax[1][1].legend(["HF","CB"])
    ax[1][1].plot([0, 1], [0 ,1],linewidth = 1, color = 'r')
    ax[1][1].set_xlabel('observed')
    ax[1][1].set_ylabel('fitted')
    ax[1][1].set_title(feat + ", fitted vs observed, p{CB} = " + str(round(cat_lm.pvalues[1],4)))
    
    
    sns.boxplot(x = "breed", 
                     y = feat,
                     hue = "breed", 
                     data = data2, 
                     #color = ["#FF8000","#0000FF"],
                     palette = "colorblind",
                     showfliers = True,
                     ax = ax[0][1])
    
    data2[feat].loc[data2["breed"]=="CB"].hist(density=True, color = 'blue', ax = ax[1][0])
    data2[feat].loc[data2["breed"]=="HF"].hist(alpha = 0.8,density=True, color = 'orange', ax = ax[1][0])
    ax[1][0].legend(["CB","HF"]) 
    ax[1][0].set_xlabel("standardised " + feat)
    ax[1][0].set_ylabel("density")
    
    plt.savefig(path_res+"cb_diff_" +feat+".png")

    
    # write to tables
    statsall[feat]["p_IC"] = round(cat_lm.pvalues[0],4)
    statsall[feat]["p_breed"] = round(cat_lm.pvalues[1],4)
    statsall[feat]["p_MY"] = round(cat_lm.pvalues[2],4)
    statsall[feat]["R2"] = round(cat_lm.rsquared,4)
    statsall[feat]["IC"] = round(cat_lm.params[0],4)
    statsall[feat]["breed"] = round(cat_lm.params[1],4)
    statsall[feat]["MY"] = round(cat_lm.params[2],4)

# filename
fn = "\Ex_breedanalysis.xlsx"
wb = "cb_stats"

# create attendance list from existing file
excel_book = load_workbook(path_res+fn)
with pd.ExcelWriter(path_res+fn, engine='openpyxl') as writer:
    writer.book = excel_book
    writer.sheets = {
        worksheet.title: worksheet
        for worksheet in excel_book.worksheets
        }
    statsall.to_excel(writer, 
                  sheet_name = wb, 
                  index = True)
    writer.save()

del writer, wb, fn, excel_book
#del data2,ax,breeds,cat_lm, cooks, outl, fitted, table, eq, feat, fig, herdids
#del herds, i, influence, lm, mybreed, plotx, q
del sfset