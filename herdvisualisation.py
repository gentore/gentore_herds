# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 09:05:36 2022

@author: adria036
-------------------------------------------------------------------------------

Script for analysis of herd data for GenTORE project

Research question: do herds with very high (>99%) and very low (<50%) HF 
                   differ from one another in terms of production characterist-
                   tics, sensor features?
                   
-------------------------------------------------------------------------------

Analysis will (1) visualise and (2) statistically compare:
    - MY / 


--> this script does the visualisations, based on which we will select the var-
    iables for statistical analsysis


"""
import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\gentore\scripts\gentore_herds") 

#%% load packages

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from openpyxl import load_workbook


#%% set constants, filepaths and read data

# set path
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data","herdanalysis")

path_res = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","results","herdanalysis")

for f in os.listdir(path):
    print(f)
    
# load data cows
cowid50 = pd.read_csv(path+"\dCOWS_50.txt",
                    index_col = 0)
cowid50["herdpart"] = 50
cowid99 = pd.read_csv(path+"\dCOWS_99.txt",
                    index_col = 0)
cowid99["herdpart"] = 99
cowid = pd.concat([cowid50,cowid99], axis = 0) 
del cowid50, cowid99

# load data herds
herds50 = pd.read_csv(path+"\dHERDS_50.txt",
                    index_col = 0)
herds50["herdpart"] = 50
herds99 = pd.read_csv(path+"\dHERDS_99.txt",
                    index_col = 0)
herds99["herdpart"] = 99
herds = pd.concat([herds50,herds99], axis = 0) 
del herds50, herds99

# load data MY
milk50 = pd.read_csv(path+"\dMY_50.txt",
                    index_col = 0)
milk50["herdpart"] = 50
milk99 = pd.read_csv(path+"\dMY_99.txt",
                    index_col = 0)
milk99["herdpart"] = 99
milk = pd.concat([milk50,milk99], axis = 0) 
del milk50, milk99

# load data SF
sf50 = pd.read_csv(path+"\dSF_50.txt",
                    index_col = 0)
sf50["herdpart"] = 50
sf99 = pd.read_csv(path+"\dSF_99.txt",
                    index_col = 0)
sf99["herdpart"] = 99
sf = pd.concat([sf50,sf99], axis = 0) 
del sf50, sf99
del path, f



#%% visualisation of the herd size and the cows in the dataset

# prepare data
sel99 = herds.loc[herds["pHF"] >= 99,:]
sel50 = herds.loc[herds["pHF"] <= 50,:]
# add indicator for which cow is HF
cowid.loc[((cowid["breed1"].str.match("HF") & (cowid["pB1"] >= 7)) | \
           (cowid["breed2"].str.match("HF") & cowid["pB2"] >= 7)),'PBHF'] = 1
cowid.loc[(cowid["PBHF"].isnull()),'PBHF'] = 0

test = cowid[["herdid","herdpart"]].groupby(by = "herdid").count()
test = test.rename(columns = {"herdpart":'no'})
test2 = cowid.loc[cowid["PBHF"] == 0, ["herdid","herdpart"]].groupby(by = "herdid").count()
test2 = test2.rename(columns = {"herdpart":"nPB"})
test = pd.merge(test,test2, how="outer", left_index=True, right_index=True)
test2 = cowid.loc[cowid["PBHF"] == 1, ["herdid","herdpart"]].groupby(by = "herdid").count()
test2 = test2.rename(columns = {"herdpart":"PB"})
test = pd.merge(test,test2, how="outer", left_index=True, right_index=True)
test.loc[test["nPB"].isnull(),'nPB'] = 0
test.loc[test["PB"].isnull(),'PB'] = 0
test = pd.merge(test,herds[["herdid","herdpart","nomax"]], left_on = "herdid",right_on = "herdid")
test["percHF"] = test["PB"]/test["no"]*100
test["cat"] = test["percHF"]*5/100
test["cat"] = np.ceil(test["cat"])
test.loc[test["cat"] == 0,"cat"] = 1

# HF and nHF cows in resp SEL50 and SEL99
cow99_HF = cowid.loc[(cowid["herdpart"] == 99) & (cowid["PBHF"] == 1),:]  #25488
cow99_nHF = cowid.loc[(cowid["herdpart"] == 99) & (cowid["PBHF"] == 0),:]  #167
cow50_HF = cowid.loc[(cowid["herdpart"] == 50) & (cowid["PBHF"] == 1),:]  #783
cow50_nHF = cowid.loc[(cowid["herdpart"] == 50) & (cowid["PBHF"] == 0),:]  #14604

nherd = cow99_nHF["herdid"].drop_duplicates() # 78 unique farms
nherd = cow50_HF["herdid"].drop_duplicates()  # 112 unique farms

plt.rc('font', size=25)
plt.rcParams["font.family"] = "serif"
plt.rcParams["font.serif"] = ["Times New Roman"]
# matplotlib.rcParams['font.family'] = 'monospace'
fig,ax = plt.subplots(nrows = 1, ncols = 1, figsize = (7,7))
plot = ax.scatter(test.loc[test["herdpart"] == 50,"nomax"], test.loc[test["herdpart"] == 50,"no"],
           s=25, marker = "^",
           c=test.loc[test["herdpart"] == 50,"cat"], cmap='coolwarm', 
           vmax = 5, vmin = 1)
cbar = fig.colorbar(plot, fraction=0.05, shrink = 0.5, 
             drawedges = False, 
             label = "% PB-HF cows",
             ticks = [1,2,3,4,5])
cbar.ax.set_yticklabels(['[0;20]', '[20;40]', '[40;60]','[60;80]','[80;100]'])
ax.scatter(test.loc[test["herdpart"] == 99,"nomax"], test.loc[test["herdpart"] == 99,"no"],
           s=30, marker = "x",
           c=test.loc[test["herdpart"] == 99,"no"], cmap='coolwarm', 
           vmax= 5, vmin = 1)
ax.plot(np.linspace(0,433,10),np.linspace(0,433,10), color = 'k',linestyle = ":")
ax.plot(np.linspace(0,433,10),np.linspace(50,50,10), color = 'k',linestyle = ":")
ax.set_xlim(0,305)
ax.set_ylim(0,440)
plt.legend(["SEL50","SEL99"])
ax.set_xlabel("Maximal herdsize")
ax.set_ylabel("Number of cows in the dataset")
ax.set_title("Herdsize vs. number of cows in the dataset, colored by %PB-HF")
# plots farmsize vs number of animals (HF vs non-HF) in the dataset

del ax, test, test2, nherd, fig, cbar






#%% visualisations part I: milk production (milk)

#------------------1. milk production per cow - avg  ---------------------
# calculate the edges- what to plot
milk50 = milk.loc[milk["herdpart"]==50,:]
milk99 = milk.loc[milk["herdpart"]==99,:]
temp50_05 = milk50[["DIM","MY","expMY"]].groupby(by = "DIM").quantile(0.05)
temp50_95 = milk50[["DIM","MY","expMY"]].groupby(by = "DIM").quantile(0.95)
temp50_avg = milk50[["DIM","MY","expMY"]].groupby(by = "DIM").mean()
temp99_05 = milk99[["DIM","MY","expMY"]].groupby(by = "DIM").quantile(0.05)
temp99_95 = milk99[["DIM","MY","expMY"]].groupby(by = "DIM").quantile(0.95)
temp99_avg = milk99[["DIM","MY","expMY"]].groupby(by = "DIM").mean()

# plot
x = milk["DIM"].drop_duplicates()
plt.rc('font', size=25)
fig,ax = plt.subplots(nrows = 2, ncols = 1, figsize = (20,16),sharex = True)
# milk yield
ax[0].fill_between(x,temp99_95["MY"],alpha = 0.7,color = "#8B008B",edgecolor="#8B008B")
ax[0].fill_between(x,temp99_05["MY"],alpha = 1, color = "white",edgecolor="#8B008B")
line1, = ax[0].plot(x,temp99_avg["MY"],lw=5,color = "#8B008B", label='SEL99') #purple
ax[0].fill_between(x,temp50_95["MY"],alpha = 0.40,color = "#0000FF",edgecolor="#0000FF")
ax[0].fill_between(x,temp50_05["MY"],color = "white",edgecolor="#0000FF")
line2, = ax[0].plot(x,temp50_avg["MY"],lw=5,color = "#0000CD", label='SEL50') #blue
ax[0].plot(x,temp50_95["MY"],lw=1,color = "#0000FF") #blue
ax[0].plot(x,temp99_95["MY"],lw=1,color = "#8B008B") #purple
ax[0].set_ylim(0,45)
ax[0].set_xlim(11,340)
ax[0].set_title("Average milk production dynamics for SEL50 and SEL99")
ax[0].set_ylabel("Daily milk yield (DMY) [kg]")
ax[0].legend(handles = [line1,line2])

# expected milk yield
ax[1].fill_between(x,temp99_95["expMY"],alpha = 0.7,color = "#8B008B",edgecolor="#8B008B")
ax[1].fill_between(x,temp99_05["expMY"],alpha = 1, color = "white",edgecolor="#8B008B")
ax[1].fill_between(x,temp50_95["expMY"],alpha = 0.40,color = "#0000FF",edgecolor="#0000FF")
ax[1].fill_between(x,temp50_05["expMY"],color = "white",edgecolor="#0000FF")
line1, = ax[1].plot(x,temp99_avg["MY"],lw=5,color = "#8B008B", label='SEL99') #purple
line2, = ax[1].plot(x,temp50_avg["MY"],lw=5,color = "#0000CD", label='SEL50') #blue
ax[1].plot(x,temp50_95["expMY"],lw=1,color = "#0000FF") #blue
ax[1].plot(x,temp99_95["expMY"],lw=1,color = "#8B008B") #purple
ax[1].set_ylim(0,45)
ax[1].set_xlim(11,340)
ax[1].set_title("Average expected milk production dynamics for SEL50 and SEL99")
ax[1].set_ylabel("Expected daily milk yield (expDMY) [kg]")
ax[1].legend(handles = [line1,line2])


#------------------1. milk production per cow - boxplot  ---------------------
# with boxplots --DMY
plt.rc('font', size=45)
plt.rcParams["font.family"] = "serif"
plt.rcParams["font.serif"] = ["Times New Roman"]
fig,ax = plt.subplots(nrows = 1, ncols = 1, figsize = (60,30),sharex = True)
sns.boxplot(x = "DIM", 
                 y = "MY",
                 hue = "herdpart", 
                 data = milk, 
                 #color = ["#56B4E9","#0072B2"],
                 palette = "colorblind",
                 showfliers = False,
                 ax = ax)
ax.plot(x-11,temp50_avg["MY"],lw=8,color = "#003366")
ax.plot(x-11,temp99_avg["MY"],lw=8,color = "#AB6D1C")
ax.set_xticks(np.arange(0, len(x)+1, 10))
ax.set_xlabel("Days in milk (DIM) [days]")
ax.set_ylabel("Daily milk yield (DMY) [kg]")
ax.set_title("Milk production dynamics for SEL50 and SEL99")

# with boxplots --expDMY
plt.rc('font', size=45)
plt.rcParams["font.family"] = "serif"
plt.rcParams["font.serif"] = ["Times New Roman"]
fig,ax = plt.subplots(nrows = 1, ncols = 1, figsize = (60,30),sharex = True)
sns.boxplot(x = "DIM", 
                 y = "expMY",
                 hue = "herdpart", 
                 data = milk, 
                 #color = ["#56B4E9","#0072B2"],
                 palette = "colorblind",
                 showfliers = False,
                 ax = ax)
ax.plot(x-11,temp50_avg["MY"],lw=8,color = "#003366")
ax.plot(x-11,temp99_avg["MY"],lw=8,color = "#AB6D1C")
ax.set_xticks(np.arange(0, len(x)+1, 10))
ax.set_xlabel("Days in milk (DIM) [days]")
ax.set_ylabel("Expected daily milk yield (expDMY) [kg]")
ax.set_title("Milk production dynamics of expected lactation curve for SEL50 and SEL99")

del line1,line2,temp50_05, temp99_05, temp50_95, temp99_95, temp50_avg
del temp99_avg, x, ax, fig, milk50, milk99


#%% calculate LnVAR to add to SF
milk["RES"] = milk["MY"] - milk["expMY"]
test = milk[["RES","cowid"]].groupby(by="cowid").var()
test["LnVAR"] = np.log(test["RES"])
test = test.drop("RES",axis = 1)
sf = sf.join(test, on = "cowid" )

del test


#%% Selection of data

# select based on inclusion/exclusion criteria:
a = sf.loc[(sf["Include"] == 0) & (sf["herdpart"] == 50),:] # 1591
a = sf.loc[(sf["Include"] == 0) & (sf["herdpart"] == 99),:] # 2477
sf = sf.loc[sf["Include"]==1,:]  # 41052 -> 36974
a = sf.loc[(sf["herdpart"] == 50),:] # 13796
a = sf.loc[(sf["herdpart"] == 99),:] # 23178


# Explore outliers + remove in sf
# prepare dataframe
outliers = pd.DataFrame().reindex_like(sf)
for variable in sf.columns:
    if variable not in ["herdpart","DIMfirst","DIMlast","NoGaps"]:
        print(variable)
        # for SEL99
        a = sf.loc[(sf["herdpart"]==99),variable].mean() + 6*sf.loc[(sf["herdpart"]==99),variable].std()
        b = sf.loc[(sf["herdpart"]==99),variable].mean() - 6*sf.loc[(sf["herdpart"]==99),variable].std()
    
        outliers.loc[((sf[variable] > a) | \
                     (sf[variable] < b)) & \
                     (sf["herdpart"] == 99), variable] = 1
        
        # for SEL50
        a = sf.loc[(sf["herdpart"]==50),variable].mean() + 6*sf.loc[(sf["herdpart"]==50),variable].std()
        b = sf.loc[(sf["herdpart"]==50),variable].mean() - 6*sf.loc[(sf["herdpart"]==50),variable].std()
    
        outliers.loc[((sf[variable] > a) | \
                     (sf[variable] < b)) & \
                     (sf["herdpart"] == 50), variable] = 1

# check which variables have outliers
outlcols = np.nansum(outliers, axis=0)
outlcols = pd.DataFrame(outlcols)
outlcols["colname"] = outliers.columns
outlcols = outlcols.loc[outlcols[0] > 0,:]
# 38 variables with at least one outlier


# check which lactation ids have outliers
outlrows = np.nansum(outliers, axis=1)
outlrows = pd.DataFrame(outlrows)
outlrows.index = sf.index
outlrows["herdpart"] = sf["herdpart"]
outlrows = outlrows.loc[outlrows[0] > 0,:]  #1511 lactations
print("number of lactations with outliers in SEL50 = " + str(len(outlrows.loc[outlrows["herdpart"]==50,:])))
print("% of lactations with outliers in SEL50 = " + str(100*len(outlrows.loc[outlrows["herdpart"]==50,:])/len(outlrows)))
print("number of lactations with outliers in SEL99 = " + str(len(outlrows.loc[outlrows["herdpart"]==99,:])))
print("% of lactations with outliers in SEL99 = " + str(100*len(outlrows.loc[outlrows["herdpart"]==99,:])/len(outlrows)))
print("Total number of lactations with outliers = " + str(len(outlrows)))
print("Percentage of lactations with outliers = " + str(len(outlrows)*100/len(sf)))

# delete outliers per variable in cow not per cow (that would delete too much data )
for variable in sf.columns:
    if variable not in ["herdpart","DIMfirst","DIMlast","NoGaps"]:
        print(variable)
        # for SEL99
        a = sf.loc[(sf["herdpart"]==99),variable].mean() + 6*sf.loc[(sf["herdpart"]==99),variable].std()
        b = sf.loc[(sf["herdpart"]==99),variable].mean() - 6*sf.loc[(sf["herdpart"]==99),variable].std()
    
        sf.loc[((sf[variable] > a) | \
                     (sf[variable] < b)) & \
                     (sf["herdpart"] == 99), variable] = np.nan
        
        # for SEL50
        a = sf.loc[(sf["herdpart"]==50),variable].mean() + 6*sf.loc[(sf["herdpart"]==50),variable].std()
        b = sf.loc[(sf["herdpart"]==50),variable].mean() - 6*sf.loc[(sf["herdpart"]==50),variable].std()
    
        sf.loc[((sf[variable] > a) | \
                     (sf[variable] < b)) & \
                     (sf["herdpart"] == 50), variable] = np.nan

# save which variables had outliers to remove
# filename
fn = "\Ex_herdanalysis.xlsx"
wb = "Outliers"
excel_book = load_workbook(path_res+fn)
with pd.ExcelWriter(path_res+fn, engine='openpyxl') as writer:
    writer.book = excel_book
    writer.sheets = {
        worksheet.title: worksheet
        for worksheet in excel_book.worksheets
        }
    outlcols.to_excel(writer, 
                  sheet_name = wb, 
                  index = True)
    writer.save()


#%% visualisations part II: sensor features (sf)

# visualise distributions of sensor features / resilience indicators
#    to this end, we make different categories, and select the most interesting 
#    features.
# parameters of the quantile regression models
set1 = ["parsQR0","parsQR1","parsQR2","parsQR3","parsQR4","parsQR5"] 
# resilience indicators
set2 = ["ExpAClag1","LnVAR"]
# lactation curve characteristics of WOOD (fit, not expected)
set3 = ["Wood305","Wood50","WoodPeakDIM","WoodPeak","WoodSlopeToPeak", "WoodPersistency"]
# characteristics of the residuals of WOOD (fit, not expected)
set4 = ["WoodAutoCorr","WoodVariance","WoodMaxResid","WoodMeanResid","WoodSkewResid","WoodMeanAbsResid"] 
# characterstics of expected lactation curve
set5 = ["ExpPeakDIM","ExpPeak","ExpSlopeToPeak","ExpPersistency"]
# residual characteristics of expected lactation curve (QR)
set6 = ["ExpRMSE","ExpPercNegative","ExpPercLower85","ExpNeg_5d","ExpNeg_10d","ExpMaxResid", \
        "ExpAutoCorr","ExpVariance","ExpMeanResid","ExpSkewResid","ExpMeanAbsResid"]
# pertubation characteristics
set7 = ["PertNoMajor","PertNoMinor","PertNoTotal","PertMajDaysRec","PertMajDaysDev",\
        "PertMinDaysRec","PertMinDaysDev","PertMajMilkLoss","PertMinMilkLoss",\
        "PertTotalLoss","PertDeepest"]


# visualise all with a boxplot - detect outliers
for variable in sf.columns:
    print(variable)
    plt.rc('font', size=14)
    fig, ax = plt.subplots(ncols=2,nrows=1,figsize = (14,7))
    sns.boxplot(x = "herdpart",
                y = variable, 
                hue = "herdpart", 
                data = sf, 
                palette = "colorblind",
                showfliers = True,
                ax = ax[0])
    sns.violinplot(x = "herdpart",
                y = variable, 
                hue = "herdpart", 
                data = sf, 
                palette = "colorblind",
                ax = ax[1])
    ax[0].set_xticks([], [])
    ax[1].set_xticks([], [])
    ax[0].set_title(variable)
    ax[1].set_title(variable)
    # save results
    plt.savefig(path_res+"\\"+"sf_withoutl_"+variable+".png")
    
   
# visualise when outliers removed
for variable in outlcols["colname"]:
    print(variable)
    plt.rc('font', size=14)
    fig, ax = plt.subplots(ncols=2,nrows=1,figsize = (14,7))
    sns.boxplot(x = "herdpart",
                y = variable, 
                hue = "herdpart", 
                data = sf, 
                palette = "colorblind",
                showfliers = True,
                ax = ax[0])
    sns.violinplot(x = "herdpart",
                y = variable, 
                hue = "herdpart", 
                data = sf, 
                palette = "colorblind",
                ax = ax[1])
    ax[0].set_xticks([], [])
    ax[1].set_xticks([], [])
   
    n = outlcols.loc[outlcols["colname"] == variable, 0].reset_index(drop=1)
    ax[0].set_title(variable + ', no. outl = ' + str(int(n[0])))
    ax[1].set_title(variable + ', no. outl = ' + str(int(n[0])))
    # save results
    plt.savefig(path_res+"\\"+"sf_bp_vp_removed_outl_"+variable+".png")
    
    
# visualise distributions set1
plt.rc('font', size=14)
cols = 3
rows = int(np.ceil(len(set1)/cols))
l=0
fig, axes = plt.subplots(rows, cols, figsize=(16,12))
for i in range(rows):
    for j in range(cols):
        if len(set1)==l:
            break
        else:
            variable = set1[l]
            a = sf.loc[(sf["herdpart"]==99),variable]
            b = sf.loc[(sf["herdpart"]==50),variable]
            axes[i][j].hist(a, density = True, color = ["#AB6D1C"], alpha = 0.6) 
            axes[i][j].hist(b, density = True, color = ["#003366"], alpha = 0.6) 
            axes[i][j].set_title(variable)
            l += 1
            
# visualise distributions set2
plt.rc('font', size=14)
cols = 2
rows = int(np.ceil(len(set2)/cols))
l=0
fig, axes = plt.subplots(rows, cols, figsize=(16,8))
for j in range(cols):
    if len(set2)==l:
        break
    else:
        variable = set2[l]
        a = sf.loc[(sf["herdpart"]==99),variable]
        b = sf.loc[(sf["herdpart"]==50),variable]
        axes[j].hist(a, density = True, color = ["#AB6D1C"], alpha = 0.6) 
        axes[j].hist(b, density = True, color = ["#003366"], alpha = 0.6) 
        axes[j].set_title(variable)
        l += 1
            
# visualise distributions set3
plt.rc('font', size=14)
cols = 3
rows = int(np.ceil(len(set3)/cols))
l=0
fig, axes = plt.subplots(rows, cols, figsize=(16,12))
for i in range(rows):
    for j in range(cols):
        if len(set3)==l:
            break
        else:
            variable = set3[l]
            a = sf.loc[(sf["herdpart"]==99),variable]
            b = sf.loc[(sf["herdpart"]==50),variable]
            axes[i][j].hist(a, density = True, color = ["#AB6D1C"], alpha = 0.6) 
            axes[i][j].hist(b, density = True, color = ["#003366"], alpha = 0.6) 
            axes[i][j].set_title(variable)
            l += 1
    
# visualise distributions set4
plt.rc('font', size=14)
cols = 3
rows = int(np.ceil(len(set4)/cols))
l=0
fig, axes = plt.subplots(rows, cols, figsize=(16,12))
for i in range(rows):
    for j in range(cols):
        if len(set4)==l:
            break
        else:
            variable = set4[l]
            a = sf.loc[(sf["herdpart"]==99),variable]
            b = sf.loc[(sf["herdpart"]==50),variable]
            axes[i][j].hist(a, density = True, color = ["#AB6D1C"], alpha = 0.6) 
            axes[i][j].hist(b, density = True, color = ["#003366"], alpha = 0.6) 
            axes[i][j].set_title(variable)
            l += 1    

# visualise distributions set5
plt.rc('font', size=14)
cols = 3
rows = int(np.ceil(len(set5)/cols))
l=0
fig, axes = plt.subplots(rows, cols, figsize=(16,12))
for i in range(rows):
    for j in range(cols):
        if len(set5)==l:
            break
        else:
            variable = set5[l]
            a = sf.loc[(sf["herdpart"]==99),variable]
            b = sf.loc[(sf["herdpart"]==50),variable]
            axes[i][j].hist(a, density = True, color = ["#AB6D1C"], alpha = 0.6) 
            axes[i][j].hist(b, density = True, color = ["#003366"], alpha = 0.6) 
            axes[i][j].set_title(variable)
            l += 1    
            
# visualise distributions set6
plt.rc('font', size=14)
cols = 3
rows = int(np.ceil(len(set6)/cols))
l=0
fig, axes = plt.subplots(rows, cols, figsize=(16,16))
for i in range(rows):
    for j in range(cols):
        if len(set6)==l:
            break
        else:
            variable = set6[l]
            a = sf.loc[(sf["herdpart"]==99),variable]
            b = sf.loc[(sf["herdpart"]==50),variable]
            axes[i][j].hist(a, density = True, color = ["#AB6D1C"], alpha = 0.6) 
            axes[i][j].hist(b, density = True, color = ["#003366"], alpha = 0.6) 
            axes[i][j].set_title(variable)
            l += 1    

# visualise distributions set7
plt.rc('font', size=14)
cols = 3
rows = int(np.ceil(len(set7)/cols))
l=0
fig, axes = plt.subplots(rows, cols, figsize=(16,16))
for i in range(rows):
    for j in range(cols):
        if len(set7)==l:
            break
        else:
            variable = set7[l]
            a = sf.loc[(sf["herdpart"]==99),variable]
            b = sf.loc[(sf["herdpart"]==50),variable]
            axes[i][j].hist(a, density = True, color = ["#AB6D1C"], alpha = 0.6) 
            axes[i][j].hist(b, density = True, color = ["#003366"], alpha = 0.6) 
            axes[i][j].set_title(variable)
            l += 1   
a.hist()


# histograms of herd summary stats
# lactation curve summaries
# sensor features and resilience indicators

#%% visualisations of normalised
# visualise all with a boxplot - detect outliers
for variable in zsf.columns:
    print(variable)
    plt.rc('font', size=14)
    fig, ax = plt.subplots(ncols=2,nrows=1,figsize = (14,7))
    sns.boxplot(x = "herdpart",
                y = variable, 
                hue = "herdpart", 
                data = zsf, 
                palette = "colorblind",
                showfliers = True,
                ax = ax[0])
    sns.violinplot(x = "herdpart",
                y = variable, 
                hue = "herdpart", 
                data = zsf, 
                palette = "colorblind",
                ax = ax[1])
    ax[0].set_xticks([], [])
    ax[1].set_xticks([], [])
    ax[0].set_title(variable)
    ax[1].set_title(variable)
    # save results
    plt.savefig(path_res+"\\"+"zsf_with_outl_"+variable+".png")
    
   
# visualise when outliers removed
for variable in outlcols["colname"]:
    print(variable)
    plt.rc('font', size=14)
    fig, ax = plt.subplots(ncols=2,nrows=1,figsize = (14,7))
    sns.boxplot(x = "herdpart",
                y = variable, 
                hue = "herdpart", 
                data = sf, 
                palette = "colorblind",
                showfliers = True,
                ax = ax[0])
    sns.violinplot(x = "herdpart",
                y = variable, 
                hue = "herdpart", 
                data = sf, 
                palette = "colorblind",
                ax = ax[1])
    ax[0].set_xticks([], [])
    ax[1].set_xticks([], [])
   
    n = outlcols.loc[outlcols["colname"] == variable, 0].reset_index(drop=1)
    ax[0].set_title(variable + ', no. outl = ' + str(int(n[0])))
    ax[1].set_title(variable + ', no. outl = ' + str(int(n[0])))
    # save results
    plt.savefig(path_res+"\\"+"sf_bp_vp_removed_outl_"+variable+".png")


#%% visualisation HF SEL90 vs HF within SEL50

# select HF cows from dataset
cowid["isHF"] = 0
cowid.loc[((cowid["breed1"].str.match('HF')) & (cowid["pB1"] >= 7)) | \
          ((cowid["breed2"].str.match('HF')) & (cowid["pB2"] >= 7)), "isHF"] = 1
    
# select HF cows
HFcow = cowid.loc[cowid["isHF"] == 1,:]
HFsf = pd.merge(sf,HFcow["cowid"], how = "inner", on = "cowid" )

# visualise all with a boxplot - detect outliers
sfset = [set1, set2, set3, set4, set5, set6, set7]
for sfn in sfset:
    for variable in sfn:
        print(variable)
        med1 = round(HFsf.loc[HFsf["herdpart"] == 50,variable].median(),3)
        med2 = round(HFsf.loc[HFsf["herdpart"] == 99,variable].median(),3)
        
        plt.rc('font', size=14)
        fig, ax = plt.subplots(ncols=2,nrows=1,figsize = (14,7))
        sns.boxplot(x = "herdpart",
                    y = variable, 
                    hue = "herdpart", 
                    data = HFsf, 
                    palette = "colorblind",
                    showfliers = True,
                    ax = ax[0])
        sns.violinplot(x = "herdpart",
                    y = variable, 
                    hue = "herdpart", 
                    data = HFsf, 
                    palette = "colorblind",
                    ax = ax[1])
        ax[0].set_xticks([], [])
        ax[1].set_xticks([], [])
        ax[0].set_title(variable + ", SEL50 = " + str(med1) + ", SEL99 = " + str(med2))
        ax[1].set_title(variable + ", SEL50 = " + str(med1) + ", SEL99 = " + str(med2))
        ax[0].grid()
        ax[1].grid()
        # save results
        plt.savefig(path_res+"\\"+"sf_onlyHF_"+variable+".png")







#%% bin
# milk50 = milk.loc[milk["herdpart"]==50,:]
# test = milk50.pivot(index = "cowid",columns = "DIM",values = "MY")
# test = test.reset_index(drop=1).reset_index(drop=1)
# milk99 = milk.loc[milk["herdpart"]==99,:]
# test2 = milk99.pivot(index = "cowid",columns = "DIM",values = "MY")
# test2 = test.reset_index(drop=1).reset_index(drop=1)
# fig,ax = plt.subplots(nrows = 2, ncols = 1, figsize = (60,30),sharex = True)
# ax[0]
# ax[0] = sns.boxplot(data = test, palette = "cool_r")

# ax[1] = sns.boxplot(data = test, palette = "Spectral")


# test = milk.pivot(index = "cowid",columns = "DIM",values = "MY")
# test = test.reset_index(drop=1).reset_index(drop=1)
# milk99 = milk.loc[milk["herdpart"]==99,:]
# test2 = milk99.pivot(index = "cowid",columns = "DIM",values = "MY")
# test2 = test.reset_index(drop=1).reset_index(drop=1)
# fig,ax = plt.subplots(nrows = 1, ncols = 1, figsize = (60,30),sharex = True)
# ax = sns.boxplot(data = test, palette = "cool_r")