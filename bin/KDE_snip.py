# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 10:32:10 2022

@author: Dyan Meuwissen
"""
#%% Kernel density estimation

#%% Import packages
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.neighbors import KernelDensity
from scipy.stats.mstats import mquantiles

#%% Start the KDE
dataframes = [expins_1_p, expins_0_18to25_p, expins_0_25_p, expins_1_m, expins_0_18to25_m, expins_0_25_m]
titles = ['successful_primi', 'unsuccessful_18to25_primi', 'unsuccessful_after25_primi', 
          'successful_multi', 'unsuccessful_18to25_multi', 'unsuccessful_after25_multi']
colours = ['navy', 'turquoise', 'lightseagreen', 'navy', 'turquoise', 'lightseagreen']
i = 0
alpha_set = 0.80 #alpha you can change for the outlier removal by kde

for dataframe in dataframes:
    data = dataframe.copy()
    title = titles[i]
    colour = colours[i]
    
    #Make a plot of the data
    with plt.style.context(("seaborn", "ggplot")):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(data['diff_probins_alarm_h'], 
                data['diff_probins_lut_h'], 'D', color = colour)
        plt.xlabel("Difference btw the probins and HN alarm in hours", color = 'black', fontweight="bold")
        plt.ylabel("Difference btw the probins and PMASC luteolysis in hours", color = 'black', fontweight="bold")
        plt.title(title, fontsize=20, fontweight="bold")
        plt.xlim([0,200])
        plt.ylim([0,200])
        plt.show()
    
    # Extract x and y
    x = np.array(data['diff_probins_alarm_h']) #trait on x-ax
    y = np.array(data['diff_probins_lut_h'])    #trait on y-ax
    
    #Create the datamatrix (X)
    X = pd.DataFrame(x)
    Y = pd.DataFrame(y).rename(columns = {0:1})
    X = pd.merge(X, Y, left_index=True, right_index=True, how = 'outer')
    
    #Drop the nans from the variable
    X = X[~np.isnan(X[0])]
    X = X[~np.isnan(X[1])]
    
    # Estimate density with a Gaussian kernel density estimator
    # You need to fit the model always on the succesful insemination group. 
    #Because you want the model that describes the density of the succesfull inseminations
    #and use this density model to see what is the log-likelihood of the unsuccesful inseminations
    #to fall within this density model.
    #You need to do this ofcourse for the primiparous and multiparous separate
    
    #only for succesful primi (i==0) and succesful multi (i==3)
    #fit model on kde
    #calculate scores
    #and calculate tau
    if i == 0:
        kde = KernelDensity(kernel='gaussian', bandwidth=1.0)
        kde.fit(X)
        kde_X = kde.score_samples(X)
        tau_kde = mquantiles(kde_X, 1. - alpha_set)
    
    if i == 3:
        kde = KernelDensity(kernel='gaussian')
        kde.fit(X)
        kde_X = kde.score_samples(X)
        tau_kde = mquantiles(kde_X, 1. - alpha_set)
        
    #Then for unsuccesful, use the kde fitted on X of succesful and tau calculated on scores of X succesful
    #First calculate scores of the unsuccesful X (with the model of succesful) and compare it to the tau of succesful
    
    #Calculate scores with the model of succesful X
    kde_X = kde.score_samples(X)
    
    #Identify the outliers by comparing scores to tau defined by succesful X
    outliers = np.argwhere(kde_X < tau_kde) 
    outliers = outliers.flatten()
    X_outliers = np.array(X)
    X_outliers = X_outliers[outliers]
    
    #Identify valid group
    normal_samples = np.argwhere(kde_X >= tau_kde)
    normal_samples = normal_samples.flatten()
    X_valid = np.array(X)
    X_valid = X_valid[normal_samples]
    
    #print summary of the data
    print(title)
    print("Original Samples : ",X.shape[0])
    print("Number of Outliers : ", len(outliers))
    print("Number of Normal Samples : ", len(normal_samples))
    
    #Plot to see the ones that should be removed
    with plt.style.context(("seaborn", "ggplot")):
        plt.scatter(X_outliers[:, 0], X_outliers[:, 1], c="tab:red", label="Outliers")        
        plt.scatter(X_valid[:, 0], X_valid[:, 1], c="tab:green", label="Valid Samples")
        plt.legend(loc="best")
        plt.title(title, fontsize=20, fontweight="bold")
        plt.xlabel("Difference btw the probins and HN alarm in hours", color = 'black', fontweight="bold")
        plt.ylabel("Difference btw the probins and PMASC luteolysis in hours", color = 'black',fontweight="bold")
        plt.xlim([0,200])
        plt.ylim([0,200])
        plt.show()
        
    #Make a dataframe out of X_valid and add a column valid
    #These are the ones to keep from the expins_0_18to25 dataframe
    X_valid = pd.DataFrame(X_valid).rename(
        columns = {0:'diff_probins_alarm_h', 1: 'diff_probins_lut_h'})
    X_valid['valid'] = 1
    
    #Merge this with data
    data = pd.merge(data, X_valid, on = [
        'diff_probins_alarm_h', 'diff_probins_lut_h'], how = 'outer')
    
    #Only keep the valid ones
    data = data[data['valid']==1]
    
    if i == 0:
        expins_1_p_valid = data.copy()
    if i == 1:
        expins_0_18to25_p_valid = data.copy()
    if i == 2:
        expins_0_25_p_valid = data.copy() 
        
    if i == 3:
        expins_1_m_valid = data.copy()
    if i == 4:
        expins_0_18to25_m_valid = data.copy()
    if i == 5:
        expins_0_25_m_valid = data.copy() 
    
    i += 1

#End for sanders