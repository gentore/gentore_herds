_# -*- coding: utf-8 -*-
"""
Created on Mon May  2 11:08:52 2022

@author: adria036

-------------------------------------------------------------------------------
Function for Kernel Density estimation



"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.neighbors import KernelDensity
from scipy.stats.mstats import mquantiles
import copy

#%% set test
"""
We want to sample 200 datapoints based on 3 variables and obtain a representative 
subsample.
AFC and meanMY seem to have a gaussian distribution, whereas no of animals in 
the dataset's distribution is right-skewed.
The chosen Kernel Density function =  Gaussian
"""
df = sHERD_99[["AFC","meanMY","no_animals","nomax","last_milk"]]
titles = ["AFC","meanMY","no_animals","nomax","last_milk"]
colours = ["navy", "turquoise", "lightseagreen"]
i=0
alpha_set = 0.8



#%% estimate kde function on x = AFC and y= pHF


data =     .copy()
title = "AFC vs. pHF"
colour = colours[i]

#--------------------------------kde estimation--------------------------------
# extract x and y
x = np.array(data['AFC'])   # trait on x-axis
y = np.array(data['pHF'])   # trait on y-axis

# create the datamatrix (X)
X = pd.DataFrame(x)
Y = pd.DataFrame(y).rename(columns = {0:1})
X = pd.merge(X, Y, left_index=True, right_index=True, how = 'outer')

# drop nans (normally there are none)
X = X[~np.isnan(X[0])]
X = X[~np.isnan(X[1])]

# prepare plot of KDE
X_plot = np.linspace(-5, 10, 1000)[:, np.newaxis]
"""
Estimate density with a Gaussian kernel density estimator
   KDE workflow:
       1) fit model on kde
       2) calculate scores
       3) calculate tau
"""

# fit the KernelDensity function
kde = KernelDensity(kernel='gaussian', bandwidth=1.0)
kde.fit(X)
kde_X = kde.score_samples(X)

tau_kde = mquantiles(kde_X, 1. - alpha_set)

x = data[:, 0]
y = data[:, 1]
xmin, xmax = -3, 3
ymin, ymax = -3, 3


# plot the data
with plt.style.context(("seaborn", "ggplot")):
    fig = plt.figure()     
    ax = fig.gca()
    ax.plot(data['AFC'], 
            data['pHF'], 'D', color = colour)
    cfset = ax.contourf(xx, yy, f, cmap='Blues')
    plt.xlabel("AFC", color = 'black', fontweight="bold")
    plt.ylabel("pHF", color = 'black', fontweight="bold")
    plt.title(title, fontsize=20, fontweight="bold")
    plt.show()

    

    
    # ax.set_xlim(xmin, xmax)
    # ax.set_ylim(ymin, ymax)
    # Contourf plot
    



    """
    From the herds with mainly HF, use kde to select 
       KDE workflow:
           1) fit model on kde
           2) calculate scores
           3) calculate tau
    """
    




