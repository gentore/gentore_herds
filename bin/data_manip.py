"""
Created by Ines Adriaens, April 20, 2022
@author: adria036, Ines Adriaens

-------------------------------------------------------------------------------

Data manipulation for dataset selection of GenTORE

-------------------------------------------------------------------------------





"""


#%% Import packages

import pandas as pd
#import numpy as np
import os
import matplotlib.pyplot as plt
import copy


#%% Set filepath and read datasets

# filepath to data 
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data/")

# filename
fn = "indicators_9.txt"

# load "cow" data into pandas dataframe
cdata = pd.read_csv(path+fn, header = None, sep = ' ')
# select columns
cdata.drop([2,3,4,9,10,16,17,20,21], axis = 1, inplace = True)
cdata.head(10)

# add colnames
cdata.columns = ["cowid","meanMY","noNA","noNotNA","calfdate","herdid",\
                 "birthdate","breed1","pB1","breed2","pB2","AFC","milksystem"]
    
# check all have AMS as milksystem = correct (yes) => var delete
test = cdata.loc[cdata.milksystem != 2,:]
cdata.drop("milksystem",inplace = True, axis = 1)
del test

# drop data of cows not complying to breed criteria
cdata = cdata.loc[((cdata["breed1"].str.match('HF')) & (cdata["pB1"] >= 7)) | \
                   ((cdata["breed2"].str.match('HF')) & (cdata["pB2"] >= 7)) | \
                   ((cdata["breed1"].str.match('HF')) & (cdata["pB1"] == 4)) | \
                   ((cdata["breed2"].str.match('HF')) & (cdata["pB2"] == 4)) | \
                   (((cdata["pB1"] >= 7) | (cdata["pB2"] >= 7)) & \
                    ((~cdata["breed1"].str.match('HF')) & (~cdata["breed2"].str.match('HF')))),:]

# drop data of cows with first calving before 2010
cdata = cdata.loc[(cdata["calfdate"] > 20100000),:] 


# read herd information
fn = "herds_summary.txt"
hdata = pd.read_csv(path+fn, header = None, sep = ' ')
hdata.columns = ["herdid","last_milk","pHF","no1991","no2000","no2001","no2002",
                "no2003","no2004","no2005","no2006","no2007","no2008",
                "no2009","no2010","no2011","no2012","no2013","no2014",
                "no2015","no2016","no2017","no2018","no2019","no2020"]
hdata.head(10)

# assumption: pHF = out of 8/8 so to get true percentage =  divide by 8*100
hdata["pHF"] = hdata["pHF"]/8*100
fig = plt.figure()
ax = fig.gca()
hdata["pHF"].hist(ax = ax)
ax.set_title("Herd data all herds, percentage Holstein-Friesian")
ax.set_xlabel("percentage HF [%]")
ax.set_ylabel("No. herds")

# check how many unique herd ids in the cdata set vs. the herd dataset 
herdids = cdata["herdid"].drop_duplicates().reset_index(drop=True)
fig = plt.figure()
ax = fig.gca()
herds_ids = cdata["herdid"].drop_duplicates()
herds_sel = pd.merge(hdata[["herdid","pHF"]],herds_ids,on = "herdid")
herds_sel["pHF"].hist(ax = ax)
ax.set_title("Herd data herds with cows, percentage Holstein-Friesian")
ax.set_xlabel("percentage HF [%]")
ax.set_ylabel("No. herds")

# number of herds with pHF <= 50 in datasets hdata and herds_sel
no_pHF50 = hdata.loc[(hdata["pHF"]<=50),:] # 315
no_pHF50f = herds_sel.loc[(herds_sel["pHF"]<=50),:]  # 196

fig = plt.figure()
ax = fig.gca()
no_pHF50f["pHF"].hist(ax = ax)
ax.set_title("Herd data herds pHF <= 50, percentage Holstein-Friesian, N=196")
ax.set_xlabel("percentage HF [%]")
ax.set_ylabel("No. herds")

# number of herds with pHF >= 99 in datasets hdata and herds_sel
no_pHF99 = hdata.loc[(hdata["pHF"]>=99),:] # 594
no_pHF99f = herds_sel.loc[(herds_sel["pHF"]>=99),:]  # 424
        # from these 424, we want to select 196 herds based on herdsize
        # and on a representative average milk production (representative sample)
        

fig = plt.figure()
ax = fig.gca()
no_pHF50f["pHF"].hist(ax = ax)
ax.set_title("Herd data herds pHF <= 50, percentage Holstein-Friesian, N=196")
ax.set_xlabel("percentage HF [%]")
ax.set_ylabel("No. herds")


# clear workspace
del fn, path

#----------------------------- selection 1-----------------------------------
"""
SELECTION 1 based on data exploration and availablility = purely based on the
    percentage HF, the herd size and the milk production.
        SUBSET1 = All herds with pHF <= 50%
        SUBSET2 = Selection of herds with pHF >= 99
    Final selection = 

"""

# selection all herds and cows from herds with pHF <= 50%
dHERD_50 = pd.merge(hdata,herds_ids,on = "herdid") # all with cowdata -> 4025
dHERD_50 = dHERD_50.loc[(dHERD_50["pHF"]<=50),:]  # with pHF <= 50 -> 196
dHERD_50 = dHERD_50.iloc[:,[0,1,2,15,16,17,18,19,20,21,22,23,24]] # all data
dHERD_50 = dHERD_50.reset_index(drop=1)  # reset index
dHERD_50["nomax"] = dHERD_50.iloc[:,3:-1].max(axis = 1) # add max no of cows
dHERD_50 = dHERD_50.loc[(dHERD_50["nomax"]>=50) & \
                        (dHERD_50["nomax"]<=300),:]  # no cow >= 50 -> 175 FINAL

# plot pHF and no cows
fig, ax = plt.subplots(nrows = 1, ncols = 2, figsize = (15,8))
dHERD_50["pHF"].hist(ax = ax[0], color = "mediumblue")
ax[0].set_title("Herds pHF <= 50, %HF, N=179",fontsize = 12)
ax[0].set_xlabel("pHF [%]")
ax[0].set_ylabel("No. herds")
dHERD_50["nomax"].hist(ax = ax[1], color = "darkblue")
ax[1].set_title("Herds pHF <= 50, no. cows/y, N=179",
                fontsize = 12)
ax[1].set_xlabel("No. of animals")
ax[1].set_ylabel("No. herds")

# select cow dataset
dCOW_50 = pd.merge(cdata,dHERD_50["herdid"]) # 5718 animals

# visualise breeds
fig, ax = plt.subplots(nrows = 1, ncols = 3, figsize = (20,8))
dCOW_50["breed1"].loc[dCOW_50["pB1"] >= 7].hist(ax = ax[0],
                                                color = "firebrick") # purebred animals
dCOW_50["breed1"].loc[dCOW_50["pB1"] < 7].hist(ax = ax[1],
                                                color = "tomato")
dCOW_50["breed2"].loc[dCOW_50["pB1"] < 7].hist(ax = ax[2],
                                                color = "tomato")
no_cows = len(dCOW_50["breed1"].loc[dCOW_50["pB1"] >= 7])
ax[0].set_title("PB cows [pB>=7], breed, N="+str(no_cows),fontsize = 12)
ax[0].set_xlabel("Breed")
ax[0].set_ylabel("No. cows")
no_cows = len(dCOW_50["breed1"].loc[dCOW_50["pB1"] == 4])
ax[1].set_title("non-PB cows [pB<7], breed1, N="+str(no_cows),
                fontsize = 12)
ax[2].set_title("non-PB cows [pB<7], breed2, N="+str(no_cows),
                fontsize = 12)
ax[1].set_xlabel("Breed")
ax[2].set_xlabel("Breed")
ax[1].set_ylabel("No. cows")
ax[2].set_ylabel("No. cows")



# summarize cow dataset by herd
hcounts = dCOW_50.groupby(by = "herdid", axis = 0).count()
hsums = dCOW_50.groupby(by = "herdid", axis = 0).sum()
hmeans = dCOW_50.groupby(by = "herdid", axis = 0).mean()

sHERD_50 = hmeans[["AFC","meanMY","pB1","pB2"]]
sHERD_50["no_animals"] = hcounts["cowid"]
sHERD_50 = sHERD_50.reset_index(drop=1)
sHERD_50["herdid"] = hmeans.index



# summarize cow dataset by breed


#%% Some data exploration

# plot no of cows dataset
hdata.hist(column = hdata.columns[3:], 
           figsize = (20,20),
           sharex = True
           )
hdata.hist(column = hdata.columns[15:], 
           figsize = (12,12),
           sharex = False
           )

# sum of all cows over the selected years need to be bigger than 10
hdata["nosum"] = hdata.iloc[:,3:].sum(axis = 1)
hdata["nomax"] = hdata.iloc[:,3:-1].max(axis = 1)
hdata["nosum"].hist()
hdata["nomax"].hist()
hdata["nomax"].loc[(hdata["nosum"]>10) & (hdata["nomax"]<400)].hist()




#%% Selection criteria and visualisations
"""
Goal = end up with multiple datasets with cows 
Selection criteria:
    HERD SUMMARIES -- hdata
    - data availablility (and how many not)
    - last_milk after 20110101
    - pHF > 90% (for purely Hostein)
    - no of animals in herd unique per year < 300
    
    COW SUMMARIES -- cdata
    - select cows from animals in HF herd dataset as selected above
    - select cows that comply with the criteria for nonHF breeds + x-breds
    - select herds based on the animals in the latter
    - summarize cow/herd chars
    - select HF farms based on cow/herd chars with KDE function (non-parametric)
"""

#------------------------------AT HERD LEVEL-----------------------------------

# last milking before 20110101 (start = 5799, sel = 5753, 99.2% )
shdata = hdata.loc[hdata["last_milk"] > 20110000,:]

# max no of unique animals => 50 & =< 300 (start = 5753, sel = 5170, 89.9%)
shdata = shdata.loc[((shdata["nomax"] >= 50) & \
                    (shdata["nomax"] <= 300)),:]
    
# holstein farms (> 90% HF blood) (start = 5170, sel = 3066, 59.3%)
HFfarms = shdata.loc[(shdata["pHF"] >= 90),:].reset_index(drop=1)  # 99%

# TODO: merge HFfarms based on herdids -> where HFfarms has cows in final dataset

nHFfarms = shdata.loc[(shdata["pHF"] < 50),:].reset_index(drop=1)
nHFdata = pd.merge(cdata,nHFfarms.iloc[:,[0,1,2,-1]], on = "herdid")
test = nHFdata["herdid"].drop_duplicates().reset_index(drop=1) # 2351 = 76.7%


#----------------------------AT MILKDATA LEVEL---------------------------------

# select cdata from herds in HFfarms (cdata 464140, sel = 316162, 68.1%)
HFdata = pd.merge(cdata,HFfarms.iloc[:,[0,1,2,-1]], on = "herdid")

# farms to select from with cows in farm dataset
test = HFdata["herdid"].drop_duplicates().reset_index(drop=1) # 2351 = 76.7%


#-------------------------SELECT CROSS/PUREBREDS-------------------------------
# select cdata from animals that comply to the "breed" / "herd" criteria
# EITHER purebred animals > 7/8 and NOT HF ==> 3540 animals
purebreds = cdata.loc[((cdata["pB1"] >= 7) & (~cdata["breed1"].str.match('HF'))) |
                      ((cdata["pB2"] >= 7) & (~cdata["breed2"].str.match('HF'))),:]
purebreds = purebreds.reset_index(drop=1)

# OR crossebred animals exactly 4/8 HF and 4/8 OTHER/KNOWN ==> 19660 animals
crossbreds = cdata.loc[((cdata["breed1"].str.match('HF')) & (cdata["pB1"] == 4)) | \
                   ((cdata["breed2"].str.match('HF')) & (cdata["pB2"] == 4)),:]
crossbreds = crossbreds.loc[(~crossbreds["breed1"].str.match('ONB')) & \
                            (~crossbreds["breed2"].str.match('ONB')) & \
                            (crossbreds["pB1"] == 4) & \
                            (crossbreds["pB2"] == 4),:]
    
#------------------------------------------------------------------------------
# join with farm summaries selected = shdata
#   remaining: 3475 (from 3540) = 98.2%
purebreds = pd.merge(purebreds,shdata.iloc[:,[0,1,2,-1]],on = "herdid")
#   remaining: 18496 (from 19660) = 94.1%
crossbreds = pd.merge(crossbreds,shdata.iloc[:,[0,1,2,-1]],on = "herdid")

#    - list of herds + parameters
herds = purebreds.append(crossbreds)  # 21971 in total
herds = herds.iloc[:,[5,11,12,13,14]].sort_values(by = "herdid")

# in total, 1914 herds are included in this dataset
countvar = herds.groupby(by = "herdid", axis = 0).count()
meanvar = herds.groupby(by = "herdid",axis = 0).mean()

# construct dataset
herds_cpb = copy.copy(meanvar)
herds_cpb["no_animals"] = countvar["AFC"]
herds_cpb["herdidx"] = herds_cpb.index
herds_cpb = herds_cpb.reset_index(drop=1)
herds_cpb = herds_cpb.rename(columns={"herdidx": "herdid"})

# delete vars
del countvar, meanvar

# plots
herds_cpb["AFC"].hist()
herds_cpb["last_milk"].hist()
herds_cpb["pHF"].hist()
herds_cpb["no_animals"].hist()
herds_cpb.loc[herds_cpb["no_animals"] < 50, "no_animals"].hist()

"""
The herds_cpb is a list of the herds with crossbred, purebred or 'both' animals
This list allows us investigate how the herds look like, and select an 'similar'
set of herds with purely HF cows. 
The variables for taking this into account are:
    - AFC
    - last milking
    - nomax (herd size)

- pHF: for analysis later: some of the animals originate from farms with > 90% HF
- 

The herds in herds_cpb also have purebred holstein animals. Summary of the
characteristics of these?
"""

# number of herds with >1 CB/PB animals AND >95% HF blood
test = herds_cpb.loc[herds_cpb["pHF"] > 95,:]  # 449/1914 = >95%HF

#


#%% Nest
# summarize the data for each farm
# #    - list of purebred herds
# herds_pb = purebreds[["herdid","breed1"]].groupby(purebreds[["herdid","breed1"]].columns.tolist(),as_index=False).size()
# herds_cb = crossbreds[["herdid","breed1"]].groupby(crossbreds[["herdid","breed1"]].columns.tolist(),as_index=False).size()

