# -*- coding: utf-8 -*-
"""
Created on Mon Nov  7 13:44:08 2022

@author: adria036
"""
import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\cKamphuis\gentore\scripts\gentore_herds") 

#%% load packages

import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from openpyxl import load_workbook
from scipy import stats
#from statsmodels.stats.anova import AnovaRM
import numpy as np
import seaborn as sns


#%% set constants, filepaths and read data

# set path
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data","breedanalysis")

path_res = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","results","breedanalysis")

breeddata = pd.read_csv(path+"\gentore_7_11.txt",sep='\t')


for f in os.listdir(path):
    print(f)
    
# load data selection 
cowidCB = pd.read_csv(path+"\dCOWS_CB.txt",
                    index_col = 0)
cowidCB["breed"] = "CB"     # cross breds
cowidCBHF = pd.read_csv(path+"\dCOWS_CB_HF.txt",
                    index_col = 0)
cowidCBHF["breed"] = "HF"    #HF pure breds
cowid1 = pd.concat([cowidCB,cowidCBHF], axis = 0) 
del cowidCB, cowidCBHF
cowidPB = pd.read_csv(path+"\dCOWS_PB.txt",
                    index_col = 0)
cowidPB["breed"] = cowidPB["breed1"]     # pure breds
cowidPBHF = pd.read_csv(path+"\dCOWS_PB_HF.txt",
                    index_col = 0)
cowidPBHF["breed"] = "HF"    #HF pure breds
cowid2 = pd.concat([cowidPB,cowidPBHF], axis = 0) 
del cowidPB, cowidPBHF

cowid = pd.concat([cowid1,cowid2], axis = 0) 
del cowid1,cowid2
cowid = pd.merge(cowid,breeddata["cowid"],how = "inner")

# select unique 
test = breeddata["cowid"].drop_duplicates().index.values
breeddata = breeddata.iloc[test,:].reset_index(drop=1)
test = cowid["cowid"].drop_duplicates().index.values
cowid = cowid.iloc[test,:].reset_index(drop=1)
del test,f

# load milk yield data
milkCB = pd.read_csv(path+"\dMY_CB.txt",
                    index_col = 0)
milkPB = pd.read_csv(path+"\dMY_PB.txt",
                    index_col = 0)
milk = pd.concat([milkCB,milkPB], axis = 0) 
del milkCB, milkPB
milk = milk.merge(cowid[["cowid","breed"]])

# add LnVAR & autocorr
milk["RES"] = milk["MY"] - milk["expMY"]
test = milk[["RES","cowid"]].groupby(by="cowid").var()
test["LnVAR"] = np.log(test["RES"])
test = test.drop("RES",axis = 1)
breeddata = breeddata.join(test, on = "cowid")
del test

cowid = cowid.merge(breeddata[["cowid","NoDays"]],how="inner")
breeddata = breeddata.merge(cowid[["cowid","breed"]])


# prepare color plot
cols = breeddata["breed"].drop_duplicates().reset_index(drop=1).to_frame()
cols["color"] = pd.DataFrame(np.linspace(0,1,len(cols)),columns = ["color"])
breeddata = pd.merge(breeddata,cols, how = "inner",on = "breed")
del cols,path

#%%  Summaries

print("total number of herds  = " + str(len(breeddata["herdid"].drop_duplicates())))
print("total number of cows in PB = " + str(len(breeddata.loc[(breeddata["breed"] != "CB"),:])))
print("total number of herds in PB = " + str(len(breeddata.loc[(breeddata["breed"] != "CB"),"herdid"].drop_duplicates())))
print("total number of cows in CB = " + str(len(breeddata.loc[(breeddata["breed"] == "CB"),:])))
print("total number of herds in CB = " + str(len(breeddata.loc[(breeddata["breed"] == "CB"),"herdid"].drop_duplicates())))


# summary stats based on breed
temp_avg = round(cowid[["breed","meanMY","NoDays","AFC"]].groupby(by = "breed").mean(),1)
temp_std = round(cowid[["breed","meanMY","NoDays","AFC"]].groupby(by = "breed").std(),1)
temp_min = round(cowid[["breed","meanMY","NoDays","AFC"]].groupby(by = "breed").min(),1)
temp_max = round(cowid[["breed","meanMY","NoDays","AFC"]].groupby(by = "breed").max(),1)
temp_cnt = cowid[["breed","cowid"]].groupby(by = ["breed"]).count()
temp_cnt2 = cowid[["breed","herdid"]].drop_duplicates().groupby(by = ["breed"]).count()

# combine and reorder
tempHERD = pd.concat([temp_avg,temp_std,temp_min,temp_max, \
                  temp_cnt,temp_cnt2], axis = 1)
    
del temp_avg,temp_std,temp_min,temp_max,temp_cnt,temp_cnt2

# filename
fn = "\Ex_breedanalysis.xlsx"
wb = "breed_overviewnew"

# create attendance list from existing file
excel_book = load_workbook(path_res+fn)
with pd.ExcelWriter(path_res+fn, engine='openpyxl') as writer:
    writer.book = excel_book
    writer.sheets = {
        worksheet.title: worksheet
        for worksheet in excel_book.worksheets
        }
    tempHERD.to_excel(writer, 
                  sheet_name = wb, 
                  index = True)
    writer.save()

del writer, wb, fn, excel_book

# summary stats of CB animals
cb = cowid.loc[cowid["breed"] == "CB",:]
cb.loc[cb["breed1"] == "HF","breed1"] = cb.loc[cb["breed1"] == "HF","breed2"]
temp_avg = round(cb[["breed1","meanMY","NoDays","AFC"]].groupby(by = "breed1").mean(),1)
temp_std = round(cb[["breed1","meanMY","NoDays","AFC"]].groupby(by = "breed1").std(),1)
temp_min = round(cb[["breed1","meanMY","NoDays","AFC"]].groupby(by = "breed1").min(),1)
temp_max = round(cb[["breed1","meanMY","NoDays","AFC"]].groupby(by = "breed1").max(),1)
temp_cnt = cb[["breed1","cowid"]].groupby(by = ["breed1"]).count()
temp_cnt2 = cb[["breed1","herdid"]].drop_duplicates().groupby(by = ["breed1"]).count()

# combine and reorder
tempHERD = pd.concat([temp_avg,temp_std,temp_min,temp_max, \
                  temp_cnt,temp_cnt2], axis = 1)
# tempHERD = tempHERD.dropna()
# tempHERD = tempHERD.loc[tempHERD["cowid"]>=50,:]

# write to excel
fn = "\Ex_breedanalysis.xlsx"
wb = "cb_overview2"
# create attendance list from existing file
excel_book = load_workbook(path_res+fn)
with pd.ExcelWriter(path_res+fn, engine='openpyxl') as writer:
    writer.book = excel_book
    writer.sheets = {
        worksheet.title: worksheet
        for worksheet in excel_book.worksheets
        }
    tempHERD.to_excel(writer, 
                  sheet_name = wb, 
                  index = True)
    writer.save()

del writer, wb, fn, excel_book


# # second dataset gerbrich



#%% statistical analysis


import statsmodels.api as sm
# import statsmodels.formula.api as smf
from statsmodels.formula.api import ols

cmap = cm.get_cmap('Spectral')
# sf list
sfset = ["ExpAClag1","LnVAR","Wood305","Wood50","WoodPeakDIM","WoodPeak",\
              "WoodSlopeToPeak", "WoodPersistency","WoodAutoCorr","WoodVariance",\
              "WoodMaxResid","WoodMeanResid","WoodSkewResid","WoodMeanAbsResid",\
              "ExpPeakDIM","ExpPeak","ExpSlopeToPeak","ExpPersistency",\
              "ExpRMSE","ExpPercNegative","ExpPercLower85","ExpNeg_5d","ExpNeg_10d",\
              "ExpMaxResid", "ExpAutoCorr","ExpVariance","ExpMeanResid",\
              "ExpSkewResid","ExpMeanAbsResid","PertNoMajor","PertNoMinor",\
              "PertNoTotal","PertMajDaysRec","PertMajDaysDev",\
              "PertMinDaysRec","PertMinDaysDev","PertMajMilkLoss","PertMinMilkLoss",\
              "PertTotalLoss","PertDeepest"]
rasset = ["FLV","MON","BS","ZRB","NRB","MRY","BBL","JER","G","FH","AYS", "cross"]
rasset2 = ["CBFLV","CBMON","CBBS","CBZRB","CBNRB","CBMRY","CBBBL","CBJER","CBG","CBFH","CBAYS"]

    
# prepare color plot
breeddata = breeddata.merge(cowid[["cowid","breed"]])
cols = breeddata["breed"].drop_duplicates().reset_index(drop=1).to_frame()
cols["color"] = pd.DataFrame(np.linspace(0,1,len(cols)),columns = ["color"])
breeddata = pd.merge(breeddata,cols, how = "outer",on = "breed")

# breeddata = breeddata.loc[breeddata["breed"] != "G",:]

statsall_n = pd.DataFrame([],columns = sfset,index = [list(["HF"])+rasset+rasset2]) #for numbers
statsall_coeff = pd.DataFrame([],columns = sfset,index = [list(["Intercept"])+rasset+list(["meanMY","R2"])])
statsall_pval =  pd.DataFrame([],columns = sfset,index = [list(["Intercept"])+rasset+list(["meanMY"])])
statsall_cilow =  pd.DataFrame([],columns = sfset,index = [list(["Intercept"])+rasset+list(["meanMY"])])
statsall_cihigh =  pd.DataFrame([],columns = sfset,index = [list(["Intercept"])+rasset+list(["meanMY"])])
for feat in sfset:
    
    #STEP0: select variables and group in "data"
    data = breeddata[list([feat]) +list(["HF"])+rasset + list(["meanMY"]) + list(["color"]) + list(["breed"])].copy()
    data = data.dropna()   # crucial
    data = data.reset_index(drop=1)

    # fit model
    #lm = sm.OLS(data[feat],data[rasset + list(["meanMY"])]).fit()
    lm = sm.OLS(data[feat],data[list(["HF"])+rasset]).fit()
    print(lm.summary())
    # create instance of influence   -- # np.set_printoptions(suppress=True)
    influence = lm.get_influence()
    # obtain Cook's distance for each observation
    cooks = influence.cooks_distance

    # select outliers based on Cook's distance larger than the threshold of q=99.5%
    q = np.quantile(cooks[0],q=0.995)
    outl = cooks[0][cooks[0] > q]
    print("number of outliers = " + str(len(outl)))

    # plot outlying observations based on threshold q
    # plt.rc('font', size=20)
    # fig,ax = plt.subplots(nrows = 2, ncols = 2, figsize = (30,25),sharex = False)
    # plotx = np.arange(1,len(cooks[0])+1,1)
    # ax[0][0].scatter(plotx,cooks[0],color = 'darkslategrey',s = 3)
    # ax[0][0].scatter(plotx[cooks[0]>q],outl,s=12,linewidth = 1,marker = "x",color = 'r')
    # ax[0][0].set_xlabel('datapoint')
    # ax[0][0].set_ylabel('Cook''s distance')
    # ax[0][0].set_title("Cook's distances, q = " + str(round(q,4)) + ", no. obs = " + str(len(outl)))

    # remove outliers with cook's distance larger than the quantile q = 0.995 
    data2 = data.iloc[np.where(cooks[0]<q)].copy()
    # min-max standardisation sf and milk yield averages
    data2[feat] = (data2[feat] - data2[feat].min()) / \
                         (data2[feat].max() - data2[feat].min())  # standardise
    data2["meanMY"] = (data2["meanMY"] - data2["meanMY"].min()) / \
                          (data2["meanMY"].max() - data2["meanMY"].min())  # standardise
                
    # run LM with categorical defined
    eq = feat + ' ~ ' 
    for ras in range(0,len(rasset)):
        eq = eq + rasset[ras] + ' + '
    eq = eq + "meanMY"
    lm = ols(eq,
             data=data2).fit()
    fitted = lm.fittedvalues
    # table = sm.stats.anova_lm(lm, typ=2) # Type 2 ANOVA DataFrame
    #print(table)
    print(lm.summary())
    
    # plot outlying observations based on threshold q
    plt.rc('font', size=20)
    fig,ax = plt.subplots(nrows = 2, ncols = 2, figsize = (30,25),sharex = False)
    plotx = np.arange(1,len(cooks[0])+1,1)
    ax[0][0].scatter(plotx,cooks[0],color = cmap(data["color"]),s = 3)
    ax[0][0].scatter(plotx[cooks[0]>q],outl,s=12,linewidth = 1,marker = "x",color = 'r')
    ax[0][0].set_xlabel('datapoint')
    ax[0][0].set_ylabel('Cook''s distance')
    ax[0][0].set_title("Cook's distances, q = " + str(round(q,4)) + ", no. obs = " + str(len(outl)))
    colors = cmap(data2["color"])
    # plot

    ax[1][1].scatter(data2[feat],fitted,s = 3, c=colors)
    
    sns.boxplot(x = "breed", 
                     y = feat,
                     hue = "breed", 
                     data = data2, 
                     #color = ["#FF8000","#0000FF"],
                     palette = "colorblind",
                     showfliers = False,
                     ax = ax[0][1])
    
    
    plt.savefig(path_res+"breeds_" +feat+".png")

    
    # write to tables
    statsall_pval[feat] = lm.pvalues.values.round(4)
    statsall_coeff[feat][0:-1] = lm.params.values.round(4)
    statsall_coeff[feat]["R2"] =lm.rsquared.round(4)
    test = lm.conf_int()
    statsall_cilow[feat] = test[0].values.round(4)
    statsall_cihigh[feat] = test[1].values.round(4)
    statsall_n[feat]["HF"] = len(data2.loc[data2["HF"]==1,"HF"])
    for ras in rasset:
        print(ras)
        statsall_n[feat][ras] = len(data2.loc[data2[ras]==1,ras])
        if ras != "cross":
            statsall_n[feat]["CB"+ras] = len(data2.loc[data2[ras]==0.5,ras])
                
        

# filename
fn = "\Ex_breedanalysis.xlsx"
wb1 = "breed_coef"
wb2 = "breed_pval"
wb3 = "breed_cilow"
wb4 = "breed_cihigh"
wb5 = "breed_n"

# create attendance list from existing file
excel_book = load_workbook(path_res+fn)
with pd.ExcelWriter(path_res+fn, engine='openpyxl') as writer:
    writer.book = excel_book
    writer.sheets = {
        worksheet.title: worksheet
        for worksheet in excel_book.worksheets
        }
    statsall_coeff.to_excel(writer, 
                  sheet_name = wb1, 
                  index = True)
    statsall_pval.to_excel(writer, 
                  sheet_name = wb2, 
                  index = True)
    statsall_cilow.to_excel(writer, 
                  sheet_name = wb3, 
                  index = True)
    statsall_cihigh.to_excel(writer, 
                  sheet_name = wb4, 
                  index = True)
    statsall_n.to_excel(writer, 
                  sheet_name = wb5, 
                  index = True)
    writer.save()

del writer, fn, excel_book
del data2,ax, cooks, outl, fitted, eq, feat, fig
del influence, lm, plotx, q
del sfset