# -*- coding: utf-8 -*-
"""
Created on Wed May 11 16:00:41 2022

@author: adria036

-------------------------------------------------------------------------------

Script to read data of selected cows, read and select lactation and sensor
feature data for:
    - dataset selected based on cow level: pure and cross breds
-------------------------------------------------------------------------------



"""

#%% Import packages

import pandas as pd
import os


#%% set  filepaths and constants

# filepath to selected cow data 
path = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data","breedanalysis/")

# filepath to sensorfeatures
path_sf = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","results/")

# filepath to milk data incl. quantile regression expected
path_my = os.path.join("C:","/Users","adria036",
                    "OneDrive - Wageningen University & Research","iAdriaens_doc",
                    "Projects","cKamphuis","gentore","data/")

# filenames for purebred and crossbreds and HF selection
fn1 = "dCOWS_CB.txt"
fn2 = "dCOWS_CB_HF.txt"
fn3 = "dCOWS_PB.txt"
fn4 = "dCOWS_PB_HF.txt"

# filenames for sf and milkyield data
fnsf = [f for f in os.listdir(path_sf) if os.path.isfile(os.path.join(path_sf,f)) \
             and "D2_CLSFqr" in f ]
fnmy = [f for f in os.listdir(path_my) if os.path.isfile(os.path.join(path_my,f)) \
             and "exp_my" in f ]


#%% read and combine data

# read cb data
crossbreds = pd.read_csv(path+fn1, index_col = 0)
cb_holstein = pd.read_csv(path+fn2, index_col = 0)
data_cb = pd.concat([crossbreds, cb_holstein])
del crossbreds, cb_holstein

# read pb data
purebreds = pd.read_csv(path+fn3, index_col = 0)
pb_holstein = pd.read_csv(path+fn4, index_col = 0)
data_pb = pd.concat([purebreds,pb_holstein])

# ids of cb and pb to select on
ids_cb = data_cb["cowid"]
ids_pb = data_pb["cowid"]


#%% load and select sf data

# loop over my and sf data
for f in fnsf:
    # read data - %timeit result: less than 1s
    temp = pd.read_csv(path_sf+f)
    if "data_sf" not in globals():
        data_sf = temp
    else:
        data_sf = pd.concat([data_sf,temp])
        del temp

# select based on ids by inner merge
sf_pb = pd.merge(data_sf,ids_pb, left_on = "CowID", right_on = "cowid")    
sf_pb = sf_pb.drop(labels = ["CowID"], axis = 1)    
sf_cb = pd.merge(data_sf,ids_cb, left_on = "CowID", right_on = "cowid")    
sf_cb = sf_cb.drop(labels = ["CowID"], axis = 1)

                   
#%% load and select milk yield data

# combine ids to select and optimize memory
ids = pd.concat([ids_pb,ids_cb])

for f in fnmy:
    #%timeit result: +/- 16s
    temp = pd.read_csv(path_my+f, header = None,
                       names = ["cowid","DIM","MY","expMY"],
                       sep = ' ', na_values="NA")
    temp = pd.merge(temp,ids, left_on = "cowid", right_on = "cowid")    
    if "data_my" not in globals():
        data_my = temp
    else:
        data_my = pd.concat([data_my,temp])
        del temp
        
# select based on ids by inner merge
my_pb = pd.merge(data_my,ids_pb, left_on = "cowid", right_on = "cowid")      
my_cb = pd.merge(data_my,ids_cb, left_on = "cowid", right_on = "cowid")    

# clear workspace
del fn1, fn2, fn3, fn4, fnmy, fnsf
del path_my, path_sf


#%% save my and sf data

# purebreds
sf_pb.to_csv(path+"dSF_PB.txt")
my_pb.to_csv(path+"dMY_PB.txt")

# crossbreds
sf_cb.to_csv(path+"dSF_CB.txt")
my_cb.to_csv(path+"dMY_CB.txt")
